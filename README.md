# FamFin-app-ws
## **Introduction**
### This project is not completed. Deadline 2020-08-23.

Welcome to the **Family Finances application web service**

This Spring Boot project was created with Java 1.8

**Content:**
- Run FamFin-app-ws on IDEA.
- Run FamFin-app-ws on Apache Tomcat.
- Requests.
    - Postman installation.
    - The basics of using Postman for API testing.
- Rest-assured tests
- FamFin application verification service
    - Email verification
    - Reset password
    

### Run FamFin-app-ws on IDEA

1. Download famfin-app-ws-master.zip.
2. Unpack to your desired location.
3. Open project favorite IDEA (Example Intellij IDEA ultimate).
4. Go to src\main\resources and rename file application.properties.edit to application.properties and open it.
5. If want use MySQL then (If want to use H2 DB go step 7):
    - Create DB in MySQL and give user all privileges. Commands for MySQL 
        - Create DB - create database DATABASE_NAME;
        - Crate user - create user 'USER_NAME'@'localhost' identified by 'PASSWORD';
        - Give all privileges user on DB - grand all privileges on DATABASE_NAME.* to 'USER_NAME'@'localhost'
    - MYSQL_USER_NAME - write MySQL user name.
    - MYSQL_PASSWORD - write MySQL password.
    - DATABASE_NAME - write database name.
  
6. Configure Gmail account for send email verification letter:
    - GMAIL_ACCOUNT - write Gmail account (example yourName@gmail.com). ATTENTION! - email address must be real.
    - GMAIL_ACCOUNT_PASSWORD - write Gmail account password. ATTENTION! - email password must be real.
    
7. if want use H2 DB
    - Comment first 3 lines (where are MYSQL_USER_NAME, MYSQL_PASSWORD and DATABASE_NAME).
    - Uncomment 3 last lines. Your H2 user and password - root
    
8. Run project like SpringBoot 

### Run FamFin-app-ws on Apache Tomcat
1. Download famfin-app-ws-master.zip.
2. Unpack to your desired location.
3. Open project favorite IDEA (Example Intellij IDEA ultimate).
4. Go to src\main\resources and rename file application.properties.edit to application.properties and open it.
5. If want use MySQL then (If want to use H2 DB go step 7):
    - Create DB in MySQL and give user all privileges. Commands for MySQL 
        - Create DB - create database DATABASE_NAME;
        - Crate user - create user 'USER_NAME'@'localhost' identified by 'PASSWORD';
        - Give all privileges user on DB - grand all privileges on DATABASE_NAME.* to 'USER_NAME'@'localhost'
    - MYSQL_USER_NAME - write MySQL user name.
    - MYSQL_PASSWORD - write MySQL password.
    - DATABASE_NAME - write database name.
  
6. Configure Gmail account for send email verification letter:
    - GMAIL_ACCOUNT - write Gmail account (example yourName@gmail.com). ATTENTION! - email address must be real.
    - GMAIL_ACCOUNT_PASSWORD - write Gmail account password. ATTENTION! - email password must be real.
    
7. if want use H2 DB
    - Comment first 3 lines (where are MYSQL_USER_NAME, MYSQL_PASSWORD and DATABASE_NAME).
    - Uncomment 3 last lines. Your H2 user and password - root

8. Open command prompt `Windows+R`, write cmd and press enter, the go to famfin-app-ws-master root catalog.
    - Example `cd C:\Users\user_name\Downloads\famfin-app-ws-master`.
9. Then run command `mvn install` and close command prompt. This command generate `famfin-app-ws-0.0.1-SNAPSHOT.war` file.
10. Go to `famfin-app-ws-0.0.1-SNAPSHOT.war` file location and rename to `app-ws.war`
11. Open Apache Tomcat and deploy `app-ws.war` from example `C:\Users\user_name\Downloads\famfin-app-ws-master\target`.
       - Install and run Apache Tomcat https://www.youtube.com/watch?v=K4ZPCwtmV1Q
       - After installation run Apache Tomcat service
       - Open on a browser this link http://localhost:8080/manager/html ant type your created credentials.
       - Deploy verification-service.war https://www.youtube.com/watch?v=zehzqr3QYfw


### Requests

Requests You can send with Postman.

**Postman installation**:

- Windows - https://www.youtube.com/watch?v=hBIY8CqVSDA
- Linux - https://www.youtube.com/watch?v=mWzKGsE7xSE
- MacOs - https://www.youtube.com/watch?v=6-TLoYKz0q0
    
**The basics of using Postman for API testing:**
- https://www.youtube.com/watch?v=t5n07Ybz7yI

**Information for API testing with Postman**

URI's abbreviations:

| Abbreviations        | Copy from                                           |
| -------------------- | --------------------------------------------------- |
| USER_ID              | "Login: response header (UserId)                    |
| AUTHORIZATION_HEADER | "Login: response header (Authorization)             |
| INCOME_TYPE_ID       | "Create income type" response body (incomeTypeId)   |
| EXPENSE_TYPE_ID      | "Create expense type" response body (expenseTypeId) |
| INCOME_ID            | "Create income" response body (incomeId)            |
| EXPENSE_ID           | "Create expense" response body (expenseId)          |


| Action                                          | Method | URL                                                                                      | Body JSON Example                                                                                       | Header KEY:VALUE                                                                            |
| ----------------------------------------------- | ------ | ---------------------------------------------------------------------------------------- |-------------------------------------------------------------------------------------------------------- |-------------------------------------------------------------------------------------------- |
| Create user                                     | POST   | http://localhost:8080/app-ws/users                                                       | { "firstName":"Vladas", "lastName":"Uponavicius", "email":"uponavicius@gmail.com", "password":"123444"} | Content-Type:application/json, Accept:application/json                                      |
| Login                                           | POST   | http://localhost:8080/app-ws/users/login                                                 | {"email":"uponavicius@gmail.com", "password":"123444"}                                                  | Content-Type:application/json, Accept:application/json                                      |
| Get user details                                | GET    | http://localhost:8080/app-ws/users/USER_ID                                               | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Update user details                             | PUT    | http://localhost:8080/app-ws/users/USER_ID                                               | {"firstName":"Vladas", "lastName":"Uponavičius"}                                                        | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Delete User                                     | DELETE | http://localhost:8080/app-ws/users/USER_ID                                               | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Create income type                              | POST   | http://localhost:8080/app-ws/users/USER_ID/incomeType                                    | {"incomeTypeName":"My incomes"}                                                                         | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Update income type                              | PUT    | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID                     | {"incomeTypeName":"My other incomes"}                                                                   | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get all income types                            | GET    | http://localhost:8080/app-ws/users/USER_ID/incomeType                                    | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get income type                                 | GET    | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID                     | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Delete income type                              | DELETE | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID                     | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Create expense type                             | POST   | http://localhost:8080/app-ws/users/USER_ID/expenseType                                   | {"expenseTypeName":"Other my expense"}                                                                  | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Update expense type                             | PUT    | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPE_ID                   | {"expenseTypeName":"Other my expenses"}                                                                 | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get all expense types                           | GET    | http://localhost:8080/app-ws/users/USER_ID/expenseType                                   | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get expense type                                | GET    | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPE_ID                   | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Delete expense type                             | DELETE | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPE_ID                   | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Create income                                   | POST   | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID/income              | {"date":"2020-06-02", "name":"Salary", "amount":1900}                                                   | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Update income                                   | PUT    | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID/income/INCOME_ID    | {"date":"2020-06-03", "name":"My salary", "amount":2900}                                                | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get all incomes                                 | GET    | http://localhost:8080/app-ws/users/USER_ID/income                                        | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get income                                      | GET    | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID/income/INCOME_ID    | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get all incomes between two days                | GET    | http://localhost:8080/app-ws/users/{userId}/income?dateFrom=2020-01-01?dateTo=2020-06-16 | None                                                 											        | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get all incomes between two days and type       | GET    | http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income?dateFrom=2020-01-01&dateTo=2020-06-16             | None                                                        | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Delete income                                   | DELETE | http://localhost:8080/app-ws/users/USER_ID/incomeType/INCOME_TYPE_ID/income/INCOME_ID    | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Create expense                                  | POST   | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPE_ID/expense           | {"date":"2020-06-02", "name":"Dell XPS 957", "amount":"200"}                                            | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Update expense                                  | PUT    | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPEID/expense/EXPENSE_ID | {"date":"2020-06-03", "name":"Dell XPS 9570", "amount":"2000"}                                          | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get all expenses                                | GET    | http://localhost:8080/app-ws/users/USER_ID/expense                                       | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get expense                                     | GET    | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPEID/expense/EXPENSE_ID | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get expenses between two days                   | GET    | http://localhost:8080/app-ws/users/USER_ID/expense?dateFrom=2020-01-01&dateTo=2020-06-16                                        | None                                                       | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get expenses between two days and type          | GET    | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPEID/expense?dateFrom=2020-01-01&dateTo=2020-06-16             | None                                                       | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Delete expense                                  | DELETE | http://localhost:8080/app-ws/users/USER_ID/expenseType/EXPENSE_TYPEID/expense/EXPENSE_ID | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get shared statistic                            | GET    | http://localhost:8080/app-ws/users/USER_ID/sharedStatistic                               | None                                                                                                    | Content-Type:application/json, Accept:application/json                                      |
| Get date for balance                            | GET    | http://localhost:8080/app-ws/users/USER_ID/balance?dateFrom=2020-01-01&dateTo=2020-06-16                                       | None                                                        | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get Incomes By Month between two days by types  | GET    | http://localhost:8080/app-ws/users/USER_ID/incomesByMonth?dateFrom=2020-01-01&dateTo=2020-06-16                                 | None                                                       | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |
| Get Expenses By Month between two days by types | GET    | http://localhost:8080/app-ws/users/USER_ID/expensesByMonth?dateFrom=2020-01-01&dateTo=2020-06-16                                | None                                                        | Authorization: AUTHORIZATION_HEADER, Content-Type:application/json, Accept:application/json |

### Rest-assured tests
Family Finances application web service rest assured tests  https://gitlab.com/uponavicius/famfin-app-ws-rest-assured-test

### FamFin application verification service
Family Finances application verification service https://gitlab.com/uponavicius/verification-service
With this app can verify email and reset password
**ATTENTION!** Verify email can without this service. With this service just more beautiful verification text in a browser.

##### Deploy FamFin-app-ws and verification-service on Apache Tomcat


##### Email verification
- Deploy FamFin-app-ws and verification-service on Apache Tomcat
- Create new user with Postman

| Action      | Method | URL                                 | Body JSON Example                                                                                       | Header KEY:VALUE                                        |
| ----------- | ------ | ------------------------------------|-------------------------------------------------------------------------------------------------------- |-------------------------------------------------------- |
| Create user | POST   | http://localhost:8080/app-ws/users  | { "firstName":"Vladas", "lastName":"Uponavicius", "email":"uponavicius@gmail.com", "password":"123444"} | Content-Type:application/json, Accept:application/json  |

- Go to Your email, open letter with subject **One last step to complete your registration with FamFinApp** and click link.

#### Reset password
- Send request for password reset with Postman

| Action         | Method | URL                                                      | Body JSON Example                 | Header KEY:VALUE                                       |
| -------------- | ------ | -------------------------------------------------------- |---------------------------------- |------------------------------------------------------- |
| Password reset | POST   |http://localhost:8080/app-ws/users/password-reset-request | {"email":"uponavicius@gmail.com"} | Content-Type:application/json, Accept:application/json |

- Go to Your email, open letter with subject **Password reset request - FamFinApp** and click link.
- Then on a browser type "New password",  "Retype new password" and click button "Save new password".
- Try to login FamFin-app-ws with a new password, using Postman

| Action  | Method | URL                                       | Body JSON Example                                      | Header KEY:VALUE                                       |
| ------- | ------ | ----------------------------------------- |------------------------------------------------------- |------------------------------------------------------- |
| Login   | GET    | http://localhost:8080/app-ws/users/login  | {"email":"uponavicius@gmail.com", "password":"123444"} | Content-Type:application/json, Accept:application/json |




