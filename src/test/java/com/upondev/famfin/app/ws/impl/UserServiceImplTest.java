package com.upondev.famfin.app.ws.impl;

import com.upondev.famfin.app.ws.exeptions.UserServiceException;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.UserRepository;
import com.upondev.famfin.app.ws.service.impl.UserServiceImpl;
import com.upondev.famfin.app.ws.service.impl.EmailServiceImpl;
import com.upondev.famfin.app.ws.shared.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @Mock
    Utils utils;

    @Mock
    EmailServiceImpl emailService;

    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;
    String userId = "qwerty123";
    String firstName = "Vladas";
    String lastName = "uponavicius";
    String encryptedPassword = "123qwerty456";
    String email = "test@test.com";
    UserEntity userEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setUserId(userId);
        userEntity.setFirstName(firstName);
        userEntity.setLastName(lastName);
        userEntity.setEncryptedPassword(encryptedPassword);
        userEntity.setEmail(email);
        userEntity.setEmailVerificationToken(utils.generateEmailVerificationToken(userId));
    }

    @Test
    void getUser() {
        when(userRepository.findByEmail(anyString())).thenReturn(userEntity); //mocking

        UserEntity userEntity = userService.getUser(email);
        assertNotNull(userEntity);
        assertEquals(1L, userEntity.getId());
        assertEquals(firstName, userEntity.getFirstName());
        assertEquals(userId, userEntity.getUserId());
        assertEquals(encryptedPassword, userEntity.getEncryptedPassword());
    }

    @Test
    void getUser_UserServiceException() {
        when(userRepository.findByEmail(anyString())).thenReturn(null); //mocking

        assertThrows(UserServiceException.class,
                () -> {
                    userService.getUser(email);
                }
        );
    }

    @Test
    void createUser() {
        when(userRepository.findByEmail(anyString())).thenReturn(null); //mocking
        when(utils.generateId(anyInt())).thenReturn(userId);
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
        Mockito.doNothing().when(emailService).verifyEmail(any(UserEntity.class));

        UserEntity storedUserDetails = userService.createUser(firstName, lastName, encryptedPassword, email);
        assertNotNull(storedUserDetails);
        assertEquals(userEntity.getFirstName(), storedUserDetails.getFirstName());
        assertEquals(userEntity.getLastName(), storedUserDetails.getLastName());
        assertEquals(userEntity.getEmail(), storedUserDetails.getEmail());
        assertNotNull(storedUserDetails.getUserId());
        verify(utils, times(1)).generateId(30);
        verify(bCryptPasswordEncoder, times(1)).encode(encryptedPassword);
        verify(userRepository, times(1)).save(any(UserEntity.class));
    }

    @Test
    void createUser_RuntimeException() {
        when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
        
        assertThrows(RuntimeException.class,
                () -> {
                    userService.createUser(firstName, lastName, encryptedPassword, email);
                }
        );
    }


}
