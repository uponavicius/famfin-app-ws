package com.upondev.famfin.app.ws.controller;


import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.service.impl.UserServiceImpl;
import com.upondev.famfin.app.ws.ui.controller.UserController;
import com.upondev.famfin.app.ws.ui.model.response.UserRest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserServiceImpl userService;

    String firstName = "Vladas";
    String lastName = "Uponavicius";
    String email = "test@test.com";
    String userId = "qwerty123";
    String encryptedPassword = "Adf85Rty65h4ds77";

    UserEntity userEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        userEntity = new UserEntity();
        userEntity.setFirstName(firstName);
        userEntity.setLastName(lastName);
        userEntity.setEmail(email);
        userEntity.setEmailVerificationStatus(Boolean.FALSE);
        userEntity.setEmailVerificationToken(null);
        userEntity.setUserId(userId);
        userEntity.setEncryptedPassword(encryptedPassword);
    }

    @Test
    void getUser() {
        when(userService.getUserByUserId(anyString())).thenReturn(userEntity);

        UserRest userRest = userController.getUser(userId);
        assertNotNull(userRest);
        assertEquals(userId, userRest.getUserId());
        assertEquals(userEntity.getFirstName(), userRest.getFirstName());
        assertEquals(userEntity.getLastName(), userRest.getLastName());
    }
}
