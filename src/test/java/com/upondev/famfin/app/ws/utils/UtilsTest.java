package com.upondev.famfin.app.ws.utils;

import com.upondev.famfin.app.ws.shared.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

// Integration tests
@ExtendWith(SpringExtension.class)
@SpringBootTest
class UtilsTest {

    @Autowired
    Utils utils;

    String userId = "qwerty123yt6540";

    @BeforeEach
    void setUp() {
    }

    @Test
    void generateId() {
        String generatedString = utils.generateId(30);
        String anotherGeneratedString = utils.generateId(30);
        assertNotNull(generatedString);
        assertEquals(30, generatedString.length());
        assertFalse(generatedString.equalsIgnoreCase(anotherGeneratedString));
    }

    @Test
    void hasTokenNotExpired() {
        boolean hasTokenExpired = Utils.hasTokenExpired(utils.generateEmailVerificationToken(userId));
        assertFalse(hasTokenExpired);
    }

    @Test
    void hasTokenExpired(){
        String expiredToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0MUB0ZXN0LmNvbSIsImV4cCI6MTUzMjc3Nzc3NX0.cdudUo3pwZLN9UiTuXiT7itpaQs6BgUPU0yWbNcz56-l1Z0476N3H_qSEHXQI5lUfaK2ePtTWJfROmf0213UJA";
        boolean hasTokenExpired = Utils.hasTokenExpired(expiredToken);
        assertTrue(hasTokenExpired);
    }
}
