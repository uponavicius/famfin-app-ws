package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.IncomeTypeRepository;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class IncomeTypeServiceImplTest {
    UserEntity userEntity;
    IncomeTypeEntity incomeTypeEntity;
    List<IncomeTypeEntity> incomeTypeEntities;

    @InjectMocks
    IncomeTypeServiceImpl incomeTypeService;

    @Mock
    IncomeTypeRepository incomeTypeRepository;

    @Mock
    ServiceUtilsImpl serviceUtils;

    @Mock
    Utils utils;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        user();
        incomeType();
    }

    @Test
    void createIncomeType() {
        when(serviceUtils.getUserEntityByUserId(anyString())).thenReturn(userEntity);
        when(incomeTypeRepository.findByIncomeTypeNameAndUserDetails(anyString(), any(UserEntity.class))).thenReturn(null);
        when(incomeTypeRepository.save(any(IncomeTypeEntity.class))).thenReturn(incomeTypeEntity);

        IncomeTypeEntity newIncomeType = incomeTypeService.createIncomeType("", "");

        assertNotNull(newIncomeType);
        verify(serviceUtils, times(1)).getUserEntityByUserId(anyString());
        verify(incomeTypeRepository, times(1)).findByIncomeTypeNameAndUserDetails(anyString(), any(UserEntity.class));
        verify(incomeTypeRepository, times(1)).save(any(IncomeTypeEntity.class));

        assertEquals(newIncomeType.getId(), incomeTypeEntity.getId());
        assertEquals(newIncomeType.getIncomeTypeId(), incomeTypeEntity.getIncomeTypeId());
        assertEquals(newIncomeType.getIncomeTypeName(), incomeTypeEntity.getIncomeTypeName());
        assertEquals(newIncomeType.getUserDetails(), incomeTypeEntity.getUserDetails());
    }

    @Test
    void updateIncomeType() {
        when(serviceUtils.getUserEntityByUserId(anyString())).thenReturn(userEntity);
        when(incomeTypeRepository.findByIncomeTypeNameAndUserDetails(anyString(), any(UserEntity.class))).thenReturn(null);
        when(serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(anyString(), anyString())).thenReturn(incomeTypeEntity);
        when(incomeTypeRepository.save(any(IncomeTypeEntity.class))).thenReturn(incomeTypeEntity);

        IncomeTypeEntity incomeType = incomeTypeService.updateIncomeType("a", "a", "Type name");
        incomeType.setIncomeTypeName("New Income Type");

        assertNotNull(incomeType);
        verify(serviceUtils, times(1)).getUserEntityByUserId(anyString());
        verify(incomeTypeRepository, times(1)).findByIncomeTypeNameAndUserDetails(anyString(), any(UserEntity.class));
        verify(serviceUtils, times(1)).getIncomeTypeEntityByUserIdAndIncomeTypeId(anyString(), anyString());
        verify(incomeTypeRepository, times(1)).save(any(IncomeTypeEntity.class));

        assertEquals(incomeType.getId(), incomeTypeEntity.getId());
        assertEquals(incomeType.getUserDetails(), incomeTypeEntity.getUserDetails());
        assertEquals(incomeType.getIncomeTypeName(), incomeTypeEntity.getIncomeTypeName());
    }

    //TODO kaip patikrinti void metodus?????
    @Test
    void deleteIncomeType() {
        when(serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(anyString(), anyString())).thenReturn(incomeTypeEntity);
        incomeTypeService.deleteIncomeType("", "");
    }

    @Test
    void getIncomeTypes() {
        incomeTypes();
        when(serviceUtils.getUserEntityByUserId(anyString())).thenReturn(userEntity);
        when(incomeTypeRepository.findAllByUserDetails(userEntity)).thenReturn(incomeTypeEntities);

        List<IncomeTypeEntity> incomeTypes = incomeTypeService.getIncomeTypes("");
        assertNotNull(incomeTypes);
        assertEquals(2, incomeTypes.size());

    }

    @Test
    void getIncomeType() {
        when(serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(anyString(), anyString())).thenReturn(incomeTypeEntity);
        IncomeTypeEntity incomeType = incomeTypeService.getIncomeType("", "");
        assertNotNull(incomeType);
        assertEquals(incomeType, incomeTypeEntity);

    }

    void user() {
        userEntity = new UserEntity();
        userEntity.setUserId("123456789");
        userEntity.setFirstName("Name");
        userEntity.setLastName("LastName");
        userEntity.setEmail("test@test.com");
        userEntity.setEncryptedPassword("123qwe456rty789uio");
        userEntity.setEmailVerificationStatus(true);
    }

    void incomeType() {
        incomeTypeEntity = new IncomeTypeEntity();
        incomeTypeEntity.setIncomeTypeName("Income Type");
        incomeTypeEntity.setIncomeTypeId("987654321");
        incomeTypeEntity.setUserDetails(userEntity);
    }

    void incomeTypes() {
        incomeTypeEntities = new ArrayList<>();
        incomeTypeEntities.add(incomeTypeEntity);
        incomeTypeEntities.add(incomeTypeEntity);
    }
}
