package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.io.entity.ExpenseEntity;
import com.upondev.famfin.app.ws.io.entity.IncomeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.ExpenseRepository;
import com.upondev.famfin.app.ws.io.repositories.IncomeRepository;
import com.upondev.famfin.app.ws.io.repositories.UserRepository;
import com.upondev.famfin.app.ws.service.StatisticService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.ServiceUtils;
import com.upondev.famfin.app.ws.ui.model.response.StatisticRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class StatisticServiceImpl implements StatisticService {

    final UserRepository userRepository;
    final IncomeServiceImpl incomeService;
    final ExpenseServiceImpl expenseService;
    final IncomeRepository incomeRepository;
    final ExpenseRepository expenseRepository;
    final ServiceUtils serviceUtils;
    final Utils utils;

    public StatisticServiceImpl(UserRepository userRepository, IncomeServiceImpl incomeService, ExpenseServiceImpl expenseService, IncomeRepository incomeRepository, ExpenseRepository expenseRepository, ServiceUtils serviceUtils, Utils utils) {
        this.userRepository = userRepository;
        this.incomeService = incomeService;
        this.expenseService = expenseService;
        this.incomeRepository = incomeRepository;
        this.expenseRepository = expenseRepository;
        this.serviceUtils = serviceUtils;
        this.utils = utils;
    }

    @Override
    public StatisticRest getSharedStatistic(String userId) {
        double currentMonthIncomes = 0;
        double currentMonthExpenses = 0;
        double allIncomes = 0;
        double allExpenses = 0;

        StatisticRest statisticRest = new StatisticRest();

        UserEntity userDetails = userRepository.findByUserId(userId);

        // Date dateFrom = utils.localDateToDate(utils.firstMonthDay());
        // Date dateTo = utils.localDateToDate(utils.lastMonthDay());

        // Current month incomes
        List<IncomeEntity> incomes = incomeRepository.findByDateBetweenAndUserDetails(utils.firstMonthDay(), utils.lastMonthDay(), userDetails);
        for (IncomeEntity income : incomes) {
            currentMonthIncomes += income.getAmount();
        }

        statisticRest.setCurrentMonthIncomes(currentMonthIncomes);

        // Current month expenses
        List<ExpenseEntity> expenses = expenseRepository.findByDateBetweenAndUserDetails(utils.firstMonthDay(), utils.lastMonthDay(), userDetails);
        for (ExpenseEntity expense : expenses) {
            currentMonthExpenses += expense.getAmount();
        }
        statisticRest.setCurrentMonthExpenses(currentMonthExpenses);

        // Current month balance
        statisticRest.setCurrentMonthBalance(currentMonthIncomes - currentMonthExpenses);

        // All incomes
        List<IncomeEntity> allUserIncomes = incomeRepository.findAllByUserDetails(userDetails);
        for (IncomeEntity income : allUserIncomes) {
            allIncomes += income.getAmount();
        }

        // All expenses
        List<ExpenseEntity> allUserExpenses = expenseRepository.findAllByUserDetails(userDetails);
        for (ExpenseEntity expense : allUserExpenses) {
            allExpenses += expense.getAmount();
        }

        statisticRest.setAllTimeBalance(allIncomes - allExpenses);

        return statisticRest;
    }

    @Override
    public StatisticRest getBalance(String userId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        // Incomes by month
        List<IncomeEntity> incomeEntities = incomeRepository.findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);
        Map<String, Double> incomesSumByMonth = new TreeMap<>();
        for (IncomeEntity income : incomeEntities) {
            String month = income.getDate().toString().substring(0, 7);
            double value = income.getAmount();

            if (incomesSumByMonth.containsKey(month)) {
                double tempValue = incomesSumByMonth.get(month);
                tempValue += value;
                incomesSumByMonth.put(month, tempValue);
            } else {
                incomesSumByMonth.put(month, value);
            }
        }

        // Expenses by month
        List<ExpenseEntity> expenseEntities = expenseRepository.findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);
        Map<String, Double> ExpensesSumByMonth = new TreeMap<>();
        for (ExpenseEntity expense : expenseEntities) {
            String month = expense.getDate().toString().substring(0, 7);
            double value = expense.getAmount();

            if (ExpensesSumByMonth.containsKey(month)) {
                double tempValue = ExpensesSumByMonth.get(month);
                tempValue += value;
                ExpensesSumByMonth.put(month, tempValue);
            } else {
                ExpensesSumByMonth.put(month, value);
            }
        }

        StatisticRest statisticRest = new StatisticRest();
        statisticRest.setIncomesByMonth(incomesSumByMonth);
        statisticRest.setExpensesByMonth(ExpensesSumByMonth);

        return statisticRest;
    }

    @Override
    public StatisticRest getIncomesStatistic(String userId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        List<IncomeEntity> incomes = incomeRepository.
                findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);

        Map<String, Double> sumByTypes = new TreeMap<>();
        for (IncomeEntity income : incomes) {
            String type = income.getIncomeTypeDetails().getIncomeTypeName();
            double value = income.getAmount();

            if (sumByTypes.containsKey(type)) {
                double tempValue = sumByTypes.get(type);
                tempValue += value;
                sumByTypes.put(type, tempValue);
            } else {
                sumByTypes.put(type, value);
            }
        }

        StatisticRest statisticRest = new StatisticRest();
        statisticRest.setIncomesStatistic(sumByTypes);

        return statisticRest;
    }

    @Override
    public StatisticRest getExpenseStatistic(String userId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        List<ExpenseEntity> expenses = expenseRepository.
                findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);

        Map<String, Double> sumByTypes = new TreeMap<>();
        for (ExpenseEntity expense : expenses) {
            String type = expense.getExpenseTypeDetails().getExpenseTypeName();
            double value = expense.getAmount();

            if (sumByTypes.containsKey(type)) {
                double tempValue = sumByTypes.get(type);
                tempValue += value;
                sumByTypes.put(type, tempValue);
            } else {
                sumByTypes.put(type, value);
            }
        }

        StatisticRest statisticRest = new StatisticRest();
        statisticRest.setExpenseStatistic(sumByTypes);

        return statisticRest;
    }


}
