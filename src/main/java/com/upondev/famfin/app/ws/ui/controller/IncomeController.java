package com.upondev.famfin.app.ws.ui.controller;

import com.upondev.famfin.app.ws.exeptions.IncomeServiceException;
import com.upondev.famfin.app.ws.io.entity.IncomeEntity;
import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.service.impl.IncomeServiceImpl;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.ServiceUtils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import com.upondev.famfin.app.ws.ui.model.request.IncomeRequestModel;
import com.upondev.famfin.app.ws.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController // Receive HTTP requests
@RequestMapping("users/{userId}")
public class IncomeController {

    final IncomeServiceImpl incomeService;
    final Utils utils;

    public IncomeController(IncomeServiceImpl incomeService, Utils utils) {
        this.incomeService = incomeService;
        this.utils = utils;
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income
    @PostMapping(path = "/incomeType/{incomeTypeId}/income")
    public IncomeRest creteIncome(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @RequestBody IncomeRequestModel incomeRequestDetails) {
        checkIncomeRequestModelData(incomeRequestDetails);

        IncomeEntity createdIncome = incomeService.createIncome(
                userId,
                incomeTypeId,
                incomeRequestDetails.getDate(),
                incomeRequestDetails.getName(),
                incomeRequestDetails.getAmount()
        );

        return new ModelMapper().map(createdIncome, IncomeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}
    @PutMapping(path = "/incomeType/{incomeTypeId}/income/{incomeId}")
    public IncomeRest updateIncome(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @PathVariable String incomeId,
            @RequestBody IncomeRequestModel incomeRequestDetails) {
        checkIncomeRequestModelData(incomeRequestDetails);

        IncomeEntity updatedIncome = incomeService.updateIncome(
                userId,
                incomeTypeId,
                incomeId,
                incomeRequestDetails.getDate(),
                incomeRequestDetails.getName(),
                incomeRequestDetails.getAmount()
        );

        return new ModelMapper().map(updatedIncome, IncomeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}
    @DeleteMapping(path = "/incomeType/{incomeTypeId}/income/{incomeId}")
    public OperationStatusModel deleteIncome(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @PathVariable String incomeId) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        incomeService.deleteIncome(userId, incomeTypeId, incomeId);

        operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return operationStatus;
    }

    // http://localhost:8080/app-ws/users/{userId}/income
    @GetMapping(path = "/income")
    public List<IncomeRest> getAllIncomes(@PathVariable String userId) {
        List<IncomeEntity> incomes = incomeService.getIncomes(userId);
        return incomeEntityToIncomeRests(incomes);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}
    @GetMapping(path = "/incomeType/{incomeTypeId}/income/{incomeId}")
    public IncomeRest getIncome(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @PathVariable String incomeId) {
        IncomeEntity income = incomeService.getIncome(userId, incomeTypeId, incomeId);
        return new ModelMapper().map(income, IncomeRest.class);
    }

    // http://localhost:8080/app-ws/users/rFQvVnRMkbtDrSCJ15xffMZqoqjjQw/incomes?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/incomes")
    public List<IncomeRest> getIncomesBetweenTwoDays(
            @PathVariable String userId,
            @RequestParam String dateFrom,
            @RequestParam String dateTo) {


        List<IncomeEntity> incomes = incomeService.getIncomesBetweenTwoDays(
                userId,
                utils.stringToLocalDate(dateFrom),
                utils.stringToLocalDate(dateTo)
        );
        return incomeEntityToIncomeRests(incomes);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}/income?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/incomeType/{incomeTypeId}/income")
    public List<IncomeRest> getIncomesBetweenTwoDaysAndType(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @RequestParam LocalDate dateFrom,
            @RequestParam LocalDate dateTo) {

        List<IncomeEntity> incomes = incomeService.
                getIncomesBetweenTwoDaysAndIncomeType(userId, incomeTypeId, dateFrom, dateTo);
        return incomeEntityToIncomeRests(incomes);
    }


    private List<IncomeRest> incomeEntityToIncomeRests(List<IncomeEntity> incomes) {
        List<IncomeRest> incomeRests = new ArrayList<>();
        if (incomes != null && !incomes.isEmpty()) {
            Type listType = new TypeToken<List<IncomeRest>>() {
            }.getType();
            incomeRests = new ModelMapper().map(incomes, listType);
        }
        return incomeRests;
    }

    private void checkIncomeRequestModelData(@RequestBody IncomeRequestModel incomeRequestDetails) {
        if (incomeRequestDetails.getDate() == null || incomeRequestDetails.getName().isEmpty())
            throw new IncomeServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
        if (incomeRequestDetails.getAmount() <= 0)
            throw new IncomeServiceException(ErrorMessages.AMOUNT_GREATER_THAN_ZERO.getErrorMessage());
    }
}
