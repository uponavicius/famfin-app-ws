package com.upondev.famfin.app.ws.exeptions;

public class UserServiceException extends RuntimeException {
    private static final long serialVersionUID = 1583065547860252448L;

    public UserServiceException(String message) {
        super(message);
    }
}
