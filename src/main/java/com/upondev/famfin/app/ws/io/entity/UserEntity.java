package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "users")
public class UserEntity implements Serializable {
    // By this class will be stored user details to database.
    private static final long serialVersionUID = -221832207756173496L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String userId;

    @Column(nullable = false, length = 50)
    @Getter
    @Setter
    private String firstName;

    @Column(nullable = false, length = 50)
    @Getter
    @Setter
    private String lastName;

    @Column(nullable = false, length = 120)
    @Getter
    @Setter
    private String email;

    @Column(nullable = false)
    @Getter
    @Setter
    private String encryptedPassword;

    @Getter
    @Setter
    private String emailVerificationToken;

    @Column(nullable = false)
    @Getter
    @Setter
    private Boolean emailVerificationStatus = false;

    // User can have a lot of income types
    @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<IncomeTypeEntity> incomeTypes;

    // User can have a lot of incomes
    @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<IncomeEntity> incomes;

    // User can have a lot of expense types
    @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<ExpenseTypeEntity> expenseTypes;

    // User can have a lot of incomes
    @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<ExpenseEntity> expenses;

}
