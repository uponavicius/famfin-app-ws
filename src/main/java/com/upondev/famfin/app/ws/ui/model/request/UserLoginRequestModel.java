package com.upondev.famfin.app.ws.ui.model.request;

public class UserLoginRequestModel {
    // This class get JSON document (userName and password) from front app and this class convert to Java class
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
