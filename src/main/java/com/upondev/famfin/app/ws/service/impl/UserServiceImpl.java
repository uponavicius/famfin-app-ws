package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.demo.DemoInfo;
import com.upondev.famfin.app.ws.exeptions.UserServiceException;
import com.upondev.famfin.app.ws.io.entity.PasswordResetTokenEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.PasswordResetTokenRepository;
import com.upondev.famfin.app.ws.io.repositories.UserRepository;
import com.upondev.famfin.app.ws.service.UserService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import com.upondev.famfin.app.ws.ui.model.response.ErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository; // For Spring Boot Data JPA

    @Autowired
    Utils utils; // For Spring Boot Data JPA

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    // https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/crypto/bcrypt/BCryptPasswordEncoder.html
    // https://en.wikipedia.org/wiki/Bcrypt

    @Autowired
    EmailServiceImpl emailService;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    ServiceUtilsImpl serviceUtils;

    @Autowired
    IncomeTypeServiceImpl incomeTypeService;

    @Autowired
    ExpenseTypeServiceImpl expenseTypeService;

    @Autowired
    IncomeServiceImpl incomeService;

    @Autowired
    ExpenseServiceImpl expenseService;

    @Autowired
    DemoInfo demoInfo;

    @Override
    public UserEntity createUser(String firstName, String lastName, String email, String password) {
        if (userRepository.findByEmail(email) != null)
            throw new RuntimeException(email + " email already registered!");

        UserEntity userEntity = new UserEntity();

        String userId = utils.generateId(30);
        userEntity.setUserId(userId);
        userEntity.setFirstName(firstName);
        userEntity.setLastName(lastName);
        userEntity.setEmail(email);
        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(password));
        userEntity.setEmailVerificationToken(utils.generateEmailVerificationToken(userId));
        userEntity.setEmailVerificationStatus(false);

        // Store data to DB
        UserEntity createdUser = userRepository.save(userEntity);

        // send an email message to user to verify their email address
        emailService.verifyEmail(createdUser);

        return createdUser;
    }

    @Override
    public UserEntity getUser(String email) {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity == null)
            throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        return userEntity;
    }

    @Override
    public UserEntity getUserByUserId(String userId) {
        UserEntity userEntity = serviceUtils.getUserEntityByUserId(userId);
        if (userEntity == null)
            throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        return userEntity;
    }

    @Override
    public UserEntity updateUser(String userId, String firstName, String lastName) {
        UserEntity userEntity = serviceUtils.getUserEntityByUserId(userId);
        if (userEntity == null)
            throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

        if (firstName.isEmpty() || lastName.isEmpty())
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        userEntity.setFirstName(firstName);
        userEntity.setLastName(lastName);

        return userRepository.save(userEntity);
    }

    @Override
    public void deleteUser(String userId) {
        UserEntity userEntity = serviceUtils.getUserEntityByUserId(userId);
        if (userEntity == null)
            throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        userRepository.delete(userEntity);
    }

    @Override
    public boolean verifyEmailToken(String token) {
        boolean isVerified = false;

        // Find user by token
        UserEntity userEntity = userRepository.findUserByEmailVerificationToken(token);

        if (userEntity != null) {
            boolean hasTokenExpired = Utils.hasTokenExpired(token);
            if (!hasTokenExpired) {
                userEntity.setEmailVerificationToken(null);
                userEntity.setEmailVerificationStatus(Boolean.TRUE);
                userRepository.save(userEntity);

                demoInfo.createDefaultIncomeTypes(userEntity);
                demoInfo.createDefaultExpenseTypes(userEntity);
                demoInfo.createDemoIncomes(userEntity);
                demoInfo.createDemoExpenses(userEntity);

                isVerified = true;
            }
        }
        return isVerified;
    }

    @Override
    public boolean requestPasswordReset(String email) {
        boolean isPasswordReset;

        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity == null) {
            return false;
        }

        String passwordResetToken = utils.generatePasswordResetToken(userEntity.getUserId());

        PasswordResetTokenEntity passwordResetTokenEntity = new PasswordResetTokenEntity();
        passwordResetTokenEntity.setToken(passwordResetToken);
        passwordResetTokenEntity.setUserDetails(userEntity);
        passwordResetTokenRepository.save(passwordResetTokenEntity);

        //Send password request email. Method must return boolean true
        isPasswordReset = emailService.sendPasswordResetRequest(
                userEntity.getFirstName(),
                userEntity.getEmail(),
                passwordResetToken);

        return isPasswordReset;
    }

    @Override
    public boolean resetPassword(String token, String password) {
        boolean isPasswordReset = false;

        if (Utils.hasTokenExpired(token)) {
            return isPasswordReset;
        }

        PasswordResetTokenEntity passwordResetTokenEntity = passwordResetTokenRepository.findByToken(token);

        if (passwordResetTokenEntity == null) {
            return isPasswordReset;
        }

        // Prepare new password
        String encodedPassword = bCryptPasswordEncoder.encode(password);

        // Update User password in database
        UserEntity userEntity = passwordResetTokenEntity.getUserDetails();
        userEntity.setEncryptedPassword(encodedPassword);
        UserEntity savedUserEntity = userRepository.save(userEntity);

        // Verify if password was saved successfully
        if (savedUserEntity != null && savedUserEntity.getEncryptedPassword().equalsIgnoreCase(encodedPassword)) {
            isPasswordReset = true;
        }

        // Remove Password Reset token from database
        passwordResetTokenRepository.delete(passwordResetTokenEntity);

        return isPasswordReset;
    }

    @Override // From UserDetailsService (Spring Security)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity == null) throw new UsernameNotFoundException(email);

        return new User(
                userEntity.getEmail(),
                userEntity.getEncryptedPassword(),
                userEntity.getEmailVerificationStatus(), // If false, then user can't login
                true, // Account not expired
                true, // Credentials not expired
                true, // Account not locked
                new ArrayList<>()
        );
    }
}
