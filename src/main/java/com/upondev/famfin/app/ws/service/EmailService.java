package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.UserEntity;

import javax.mail.MessagingException;

public interface EmailService {

    void verifyEmail(UserEntity userEntity);

    boolean sendPasswordResetRequest(String firstName, String email, String token);

    void sendEmail(String email, String subject, String textBody) throws MessagingException;

}
