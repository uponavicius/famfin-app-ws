package com.upondev.famfin.app.ws.exeptions;

public class ExpenseServiceException extends RuntimeException {
    private static final long serialVersionUID = 5090619475067474654L;

    public ExpenseServiceException(String message) {
        super(message);
    }
}
