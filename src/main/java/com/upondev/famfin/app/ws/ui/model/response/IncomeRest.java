package com.upondev.famfin.app.ws.ui.model.response;

import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;

import java.time.LocalDate;
import java.util.Date;

public class IncomeRest {
    private String incomeId;
    private LocalDate date;
    private String name;
    private double amount;
    private String incomeTypeId;

    public String getIncomeId() {
        return incomeId;
    }

    public void setIncomeId(String incomeId) {
        this.incomeId = incomeId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getIncomeTypeId() {
        return incomeTypeId;
    }

    public void setIncomeTypeId(String incomeTypeId) {
        this.incomeTypeId = incomeTypeId;
    }
}
