package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;

import java.util.List;

public interface ExpenseTypeService {

    ExpenseTypeEntity createExpenseType(String userId, String expenseTypeName);

    ExpenseTypeEntity updateExpenseType(String userId, String expenseTypeId, String expenseTypeName);

    void deleteExpenseType(String userId, String expenseTypeId);

    List<ExpenseTypeEntity> getExpenseTypes(String userId);

    ExpenseTypeEntity getExpenseTypeById(String userId, String expenseTypeId);

}
