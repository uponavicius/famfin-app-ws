package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.io.entity.ExpenseEntity;
import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.ExpenseRepository;
import com.upondev.famfin.app.ws.io.repositories.ExpenseTypeRepository;
import com.upondev.famfin.app.ws.service.ExpenseService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    final ExpenseRepository expenseRepository;
    final ExpenseTypeRepository expenseTypeRepository;
    final Utils utils;
    final ServiceUtilsImpl serviceUtils;

    public ExpenseServiceImpl(ExpenseRepository expenseRepository, ExpenseTypeRepository expenseTypeRepository, Utils utils, ServiceUtilsImpl serviceUtils) {
        this.expenseRepository = expenseRepository;
        this.expenseTypeRepository = expenseTypeRepository;
        this.utils = utils;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public ExpenseEntity createExpense(String userId, String expenseTypeId, LocalDate date, String name, double amount) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        ExpenseTypeEntity expenseTypeDetails = serviceUtils.
                getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);

        ExpenseEntity expenseEntity = new ExpenseEntity();
        expenseEntity.setExpenseId(utils.generateId(30));
        expenseEntity.setDate(date);
        expenseEntity.setName(name);
        expenseEntity.setAmount(amount);
        expenseEntity.setUserDetails(userDetails);
        expenseEntity.setExpenseTypeDetails(expenseTypeDetails);

        return expenseRepository.save(expenseEntity);
    }

    @Override
    public ExpenseEntity updateExpense(String userId, String expenseTypeId, String expenseId, LocalDate date, String name, double amount) {
        ExpenseEntity expenseEntity = serviceUtils.
                getExpenseEntityByUserIdAndExpenseId(userId, expenseId);

        ExpenseTypeEntity expenseTypeEntity = serviceUtils.getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);

        expenseEntity.setDate(date);
        expenseEntity.setName(name);
        expenseEntity.setAmount(amount);
        expenseEntity.setExpenseTypeDetails(expenseTypeEntity);

        return expenseRepository.save(expenseEntity);
    }

    @Override
    public void delete(String userId, String expenseTypeId, String expenseId) {
        ExpenseEntity expenseEntity = serviceUtils.
                getExpenseEntityByUserIdAndExpenseTypeIdAndExpenseId(userId, expenseTypeId, expenseId);
        expenseRepository.delete(expenseEntity);

    }

    @Override
    public List<ExpenseEntity> getExpenses(String userId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return expenseRepository.findAllByUserDetails(userDetails);
    }

    @Override
    public ExpenseEntity getExpense(String userId, String expenseTypeId, String expenseId) {
        return serviceUtils.
                getExpenseEntityByUserIdAndExpenseTypeIdAndExpenseId(userId, expenseTypeId, expenseId);
    }

    @Override
    public List<ExpenseEntity> getExpensesBetweenTwoDays(String userId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return expenseRepository.findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);
    }

    @Override
    public List<ExpenseEntity> getExpensesBetweenTwoDaysAndExpenseType(String userId, String expenseTypeId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        ExpenseTypeEntity expenseTypeDetails = serviceUtils.
                getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);

        return expenseRepository.
                findByDateBetweenAndUserDetailsAndExpenseTypeDetails(
                        dateFrom, dateTo, userDetails, expenseTypeDetails);
    }

}

