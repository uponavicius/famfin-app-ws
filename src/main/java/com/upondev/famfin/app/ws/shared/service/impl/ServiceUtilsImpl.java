package com.upondev.famfin.app.ws.shared.service.impl;

import com.upondev.famfin.app.ws.exeptions.*;
import com.upondev.famfin.app.ws.io.entity.*;
import com.upondev.famfin.app.ws.io.repositories.*;
import com.upondev.famfin.app.ws.shared.service.ServiceUtils;
import com.upondev.famfin.app.ws.ui.model.response.ErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceUtilsImpl implements ServiceUtils {
    @Autowired
    IncomeRepository incomeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    IncomeTypeRepository incomeTypeRepository;

    @Autowired
    ExpenseTypeRepository expenseTypeRepository;

    @Autowired
    ExpenseRepository expenseRepository;

    @Override
    public UserEntity getUserEntityByUserId(String userId) {
        UserEntity userDetails = userRepository.findByUserId(userId);
        if (userDetails == null)
            throw new UserServiceException(ErrorMessages.NO_USER_FOUND.getErrorMessage());
        return userDetails;
    }

    @Override
    public IncomeEntity getIncomeEntityByUserIdAndIncomeId(String userId, String incomeId) {
        UserEntity userDetails = getUserEntityByUserId(userId);

        IncomeEntity incomeEntity = incomeRepository.findByIncomeIdAndUserDetails(incomeId, userDetails);
        if (incomeEntity == null)
            throw new IncomeServiceException(ErrorMessages.NO_INCOME_FOUND.getErrorMessage());
        return incomeEntity;
    }

    @Override
    public IncomeTypeEntity getIncomeTypeEntityByUserIdAndIncomeTypeId(String userId, String incomeTypeId) {
        UserEntity userDetails = getUserEntityByUserId(userId);

        IncomeTypeEntity incomeTypeDetails = incomeTypeRepository.findByIncomeTypeIdAndUserDetails(incomeTypeId, userDetails);

        if (incomeTypeDetails == null)
            throw new IncomeTypeServiceException(ErrorMessages.NO_INCOME_TYPE_FOUND.getErrorMessage());
        return incomeTypeDetails;
    }

    @Override
    public IncomeEntity getIncomeEntityByUserIdAndIncomeTypeIdAndIncomeId(String userId, String incomeTypeId, String incomeId) {
        UserEntity userDetails = getUserEntityByUserId(userId);
        IncomeTypeEntity incomeTypeDetails = getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);

        IncomeEntity incomeEntity = incomeRepository.
                findByIncomeIdAndIncomeTypeDetailsAndUserDetails(incomeId, incomeTypeDetails, userDetails);
        if (incomeEntity == null)
            throw new IncomeServiceException(ErrorMessages.NO_INCOME_FOUND.getErrorMessage());
        return incomeEntity;
    }

    @Override
    public ExpenseTypeEntity getExpenseTypeEntityByUserIdAndExpenseTypeId(String userId, String expenseTypeId) {
        UserEntity userDetails = getUserEntityByUserId(userId);
        ExpenseTypeEntity expenseTypeDetails = expenseTypeRepository.findByExpenseTypeIdAndUserDetails(expenseTypeId, userDetails);
        if (expenseTypeDetails == null)
            throw new ExpenseTypeServiceException(ErrorMessages.NO_EXPENSE_TYPE_FOUND.getErrorMessage());
        return expenseTypeDetails;
    }

    @Override
    public ExpenseEntity getExpenseEntityByUserIdAndExpenseId(String userId, String incomeId) {
        UserEntity userDetails = getUserEntityByUserId(userId);

        ExpenseEntity expenseEntity = expenseRepository.findByExpenseIdAndUserDetails(incomeId, userDetails);

        if (expenseEntity == null)
            throw  new ExpenseServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

        return expenseEntity;
    }

    @Override
    public ExpenseEntity getExpenseEntityByUserIdAndExpenseTypeIdAndExpenseId(String userId, String expenseTypeId, String expenseId) {
        UserEntity userDetails = getUserEntityByUserId(userId);
        ExpenseTypeEntity expenseTypeDetails = getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);

        ExpenseEntity expenseEntity = expenseRepository.
                findByExpenseIdAndExpenseTypeDetailsAndUserDetails(expenseId, expenseTypeDetails, userDetails);
        if (expenseEntity == null)
            throw new ExpenseServiceException(ErrorMessages.NO_EXPENSE_FOUND.getErrorMessage());
        return expenseEntity;
    }


}
