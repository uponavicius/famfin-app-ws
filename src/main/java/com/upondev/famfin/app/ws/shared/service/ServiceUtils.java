package com.upondev.famfin.app.ws.shared.service;

import com.upondev.famfin.app.ws.io.entity.*;

public interface ServiceUtils {
    UserEntity getUserEntityByUserId(String userId);

    IncomeEntity getIncomeEntityByUserIdAndIncomeId(String userId, String incomeId);

    IncomeTypeEntity getIncomeTypeEntityByUserIdAndIncomeTypeId(String userId, String incomeTypeId);

    IncomeEntity getIncomeEntityByUserIdAndIncomeTypeIdAndIncomeId(String userId, String incomeTypeId, String incomeId);

    ExpenseTypeEntity getExpenseTypeEntityByUserIdAndExpenseTypeId(String userId, String expenseTypeId);

    ExpenseEntity getExpenseEntityByUserIdAndExpenseId(String userId, String incomeId);

    ExpenseEntity getExpenseEntityByUserIdAndExpenseTypeIdAndExpenseId(String userId, String expenseTypeId, String expenseId);

}


