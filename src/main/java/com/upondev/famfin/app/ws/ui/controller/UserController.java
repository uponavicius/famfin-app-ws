package com.upondev.famfin.app.ws.ui.controller;

import com.upondev.famfin.app.ws.exeptions.UserServiceException;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.service.UserService;
import com.upondev.famfin.app.ws.service.impl.IncomeTypeServiceImpl;
import com.upondev.famfin.app.ws.ui.model.request.PasswordResetModel;
import com.upondev.famfin.app.ws.ui.model.request.PasswordResetRequestModel;
import com.upondev.famfin.app.ws.ui.model.request.UserDetailsRequestModel;
import com.upondev.famfin.app.ws.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController // Receive HTTP requests
@RequestMapping("users")
public class UserController { // this class will be sent http requests  http://localhost:8080/app-ws/users

    final UserService userService;
    final IncomeTypeServiceImpl incomeTypeService;

    public UserController(UserService userService, IncomeTypeServiceImpl incomeTypeService) {
        this.userService = userService;
        this.incomeTypeService = incomeTypeService;
    }

    @PostMapping()
    public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) {
        if (userDetails.getFirstName().isEmpty() ||
                userDetails.getLastName().isEmpty() ||
                userDetails.getEmail().isEmpty() ||
                userDetails.getPassword().isEmpty())
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        UserEntity createdUser = userService.createUser(
                userDetails.getFirstName(),
                userDetails.getLastName(),
                userDetails.getEmail(),
                userDetails.getPassword());

        return new ModelMapper().map(createdUser, UserRest.class);
    }

    @GetMapping(path = "/{id}") // public userId
    public UserRest getUser(@PathVariable String email) {
        UserEntity userEntity = userService.getUserByUserId(email);
        return new ModelMapper().map(userEntity, UserRest.class);
    }

    @PutMapping(path = "/{id}")
    public UserRest updateUser(@RequestBody UserDetailsRequestModel userDetails, @PathVariable String id) {
        UserEntity updatedUser = userService.updateUser(id, userDetails.getFirstName(), userDetails.getLastName());
        return new ModelMapper().map(updatedUser, UserRest.class);
    }

    // TODO jei neistrina vistiek padaro success
    @DeleteMapping(path = "/{id}")
    public OperationStatusModel deleteUser(@PathVariable String id) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        userService.deleteUser(id);

        operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return operationStatus;
    }

    //http://localhost:8080/app-ws/users/email-verification?token=sdfsdf
    // from web - http://localhost:8080/verification-service/email-verification.html?token=
    @GetMapping(path = "/email-verification")
    public OperationStatusModel verifyEmailToken(@RequestParam(value = "token") String token) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.VERIFY_EMAIL.name());

        if (userService.verifyEmailToken(token)) {
            operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        } else {
            operationStatus.setOperationResult(RequestOperationStatus.ERROR.name());
        }
        return operationStatus;
    }

    //http://localhost:8080/app-ws/users/password-reset-request
    @PostMapping(path = "/password-reset-request")
    public OperationStatusModel requestReset(@RequestBody PasswordResetRequestModel passwordResetRequestModel) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        boolean operationResult = userService.requestPasswordReset(passwordResetRequestModel.getEmail());

        operationStatus.setOperationName(RequestOperationName.REQUEST_PASSWORD_RESET.name());
        operationStatus.setOperationResult(RequestOperationStatus.ERROR.name());

        if (operationResult) {
            operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        }
        return operationStatus;
    }


    @PostMapping(path = "/password-reset")
    public OperationStatusModel resetPassword(@RequestBody PasswordResetModel passwordResetModel) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        boolean operationResult = userService.resetPassword(
                passwordResetModel.getToken(),
                passwordResetModel.getPassword());

        operationStatus.setOperationName(RequestOperationName.PASSWORD_RESET.name());
        operationStatus.setOperationResult(RequestOperationStatus.ERROR.name());

        if (operationResult) {
            operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        }
        return operationStatus;
    }

}
