package com.upondev.famfin.app.ws.ui.controller;

import com.upondev.famfin.app.ws.service.impl.StatisticServiceImpl;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.ui.model.response.StatisticRest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users/{userId}")
public class StatisticController {

    final StatisticServiceImpl statisticService;
    final Utils utils;

    public StatisticController(StatisticServiceImpl statisticService, Utils utils) {
        this.statisticService = statisticService;
        this.utils = utils;
    }

    // http://localhost:8080/app-ws/users/{userId}/sharedStatistic
    @GetMapping(path = "/sharedStatistic")
    public StatisticRest getSharedStatistic(@PathVariable String userId) {
        return statisticService.getSharedStatistic(userId);
    }

    // http://localhost:8080/app-ws/users/{userId}/balance?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/balance")
    public StatisticRest getBalanceBetweenTwoDays(@PathVariable String userId,
                                                  @RequestParam String dateFrom,
                                                  @RequestParam String dateTo) {
        return statisticService.getBalance(userId, utils.stringToLocalDate(dateFrom), utils.stringToLocalDate(dateTo));
    }


    // http://localhost:8080/app-ws/users/{userId}/incomesStatistic?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/incomesStatistic")
    public StatisticRest getIncomesStatisticBetweenTwoDaysByTypes(
            @PathVariable String userId,
            @RequestParam String dateFrom,
            @RequestParam String dateTo) {
        return statisticService.getIncomesStatistic(userId, utils.stringToLocalDate(dateFrom), utils.stringToLocalDate(dateTo));
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseStatistic?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/expenseStatistic")
    public StatisticRest getExpensesStatisticBetweenTwoDaysByTypes(
            @PathVariable String userId,
            @RequestParam String dateFrom,
            @RequestParam String dateTo) {
        return statisticService.getExpenseStatistic(userId, utils.stringToLocalDate(dateFrom), utils.stringToLocalDate(dateTo));
    }
}
