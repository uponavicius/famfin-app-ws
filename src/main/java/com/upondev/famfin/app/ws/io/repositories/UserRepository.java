package com.upondev.famfin.app.ws.io.repositories;

import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    // Create UserRepository interface extends CrudRepository. And add annotation @Repository.
    // Take data from UserEntity class and will persist data to database.
    // This class for Spring Boot Data JPA.

    UserEntity findByEmail(String email);

    UserEntity findByUserId(String userId);

    UserEntity findUserByEmailVerificationToken(String token);
}
