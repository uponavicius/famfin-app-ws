package com.upondev.famfin.app.ws.demo;

import com.upondev.famfin.app.ws.io.entity.*;
import com.upondev.famfin.app.ws.io.repositories.ExpenseRepository;
import com.upondev.famfin.app.ws.io.repositories.ExpenseTypeRepository;
import com.upondev.famfin.app.ws.io.repositories.IncomeRepository;
import com.upondev.famfin.app.ws.io.repositories.IncomeTypeRepository;
import com.upondev.famfin.app.ws.shared.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DemoInfo {

    @Autowired
    IncomeTypeRepository incomeTypeRepository;

    @Autowired
    Utils utils;

    @Autowired
    IncomeRepository incomeRepository;

    @Autowired
    ExpenseTypeRepository expenseTypeRepository;

    @Autowired
    ExpenseRepository expenseRepository;

    public void createDefaultExpenseTypes(UserEntity userDetails) {
        ExpenseTypeEntity carMaintenance = new ExpenseTypeEntity();
        carMaintenance.setUserDetails(userDetails);
        carMaintenance.setExpenseTypeName("Car Maintenance");
        carMaintenance.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(carMaintenance);

        ExpenseTypeEntity children = new ExpenseTypeEntity();
        children.setUserDetails(userDetails);
        children.setExpenseTypeName("Children");
        children.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(children);

        ExpenseTypeEntity ClothingAndFootwear = new ExpenseTypeEntity();
        ClothingAndFootwear.setUserDetails(userDetails);
        ClothingAndFootwear.setExpenseTypeName("Clothing and Footwear");
        ClothingAndFootwear.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(ClothingAndFootwear);

        ExpenseTypeEntity communications = new ExpenseTypeEntity();
        communications.setUserDetails(userDetails);
        communications.setExpenseTypeName("Communications");
        communications.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(communications);

        ExpenseTypeEntity credit = new ExpenseTypeEntity();
        credit.setUserDetails(userDetails);
        credit.setExpenseTypeName("Credit");
        credit.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(credit);

        ExpenseTypeEntity food = new ExpenseTypeEntity();
        food.setUserDetails(userDetails);
        food.setExpenseTypeName("Food");
        food.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(food);

        ExpenseTypeEntity fuel = new ExpenseTypeEntity();
        fuel.setUserDetails(userDetails);
        fuel.setExpenseTypeName("Fuel");
        fuel.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(fuel);

        ExpenseTypeEntity giftsAndGuests = new ExpenseTypeEntity();
        giftsAndGuests.setUserDetails(userDetails);
        giftsAndGuests.setExpenseTypeName("Gifts and Guests");
        giftsAndGuests.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(giftsAndGuests);

        ExpenseTypeEntity health = new ExpenseTypeEntity();
        health.setUserDetails(userDetails);
        health.setExpenseTypeName("Health");
        health.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(health);

        ExpenseTypeEntity home = new ExpenseTypeEntity();
        home.setUserDetails(userDetails);
        home.setExpenseTypeName("Home");
        home.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(home);

        ExpenseTypeEntity husbandPersonalExpenses = new ExpenseTypeEntity();
        husbandPersonalExpenses.setUserDetails(userDetails);
        husbandPersonalExpenses.setExpenseTypeName("Husband Personal Expenses");
        husbandPersonalExpenses.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(husbandPersonalExpenses);

        ExpenseTypeEntity kindergarten = new ExpenseTypeEntity();
        kindergarten.setUserDetails(userDetails);
        kindergarten.setExpenseTypeName("Kindergarten");
        kindergarten.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(kindergarten);

        ExpenseTypeEntity otherExpenses = new ExpenseTypeEntity();
        otherExpenses.setUserDetails(userDetails);
        otherExpenses.setExpenseTypeName("Other Expenses");
        otherExpenses.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(otherExpenses);

        ExpenseTypeEntity otherProducts = new ExpenseTypeEntity();
        otherProducts.setUserDetails(userDetails);
        otherProducts.setExpenseTypeName("Other products");
        otherProducts.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(otherProducts);

        ExpenseTypeEntity entertainments = new ExpenseTypeEntity();
        entertainments.setUserDetails(userDetails);
        entertainments.setExpenseTypeName("Entertainments");
        entertainments.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(entertainments);

        ExpenseTypeEntity sport = new ExpenseTypeEntity();
        sport.setUserDetails(userDetails);
        sport.setExpenseTypeName("Sport");
        sport.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(sport);

        ExpenseTypeEntity helpForParents = new ExpenseTypeEntity();
        helpForParents.setUserDetails(userDetails);
        helpForParents.setExpenseTypeName("Help for Parents");
        helpForParents.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(helpForParents);

        ExpenseTypeEntity wifePersonalExpenses = new ExpenseTypeEntity();
        wifePersonalExpenses.setUserDetails(userDetails);
        wifePersonalExpenses.setExpenseTypeName("Wife Personal Expenses");
        wifePersonalExpenses.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(wifePersonalExpenses);

        ExpenseTypeEntity rent = new ExpenseTypeEntity();
        rent.setUserDetails(userDetails);
        rent.setExpenseTypeName("Rent");
        rent.setExpenseTypeId(utils.generateId(30));
        expenseTypeRepository.save(rent);

    }

    public void createDefaultIncomeTypes(UserEntity userDetails) {
        IncomeTypeEntity husbandIncomes = new IncomeTypeEntity();
        husbandIncomes.setUserDetails(userDetails);
        husbandIncomes.setIncomeTypeName("Husband Incomes");
        husbandIncomes.setIncomeTypeId(utils.generateId(30));
        incomeTypeRepository.save(husbandIncomes);

        IncomeTypeEntity wifeIncomes = new IncomeTypeEntity();
        wifeIncomes.setUserDetails(userDetails);
        wifeIncomes.setIncomeTypeName("Wife Incomes");
        wifeIncomes.setIncomeTypeId(utils.generateId(30));
        incomeTypeRepository.save(wifeIncomes);

        IncomeTypeEntity otherHusbandIncomes = new IncomeTypeEntity();
        otherHusbandIncomes.setUserDetails(userDetails);
        otherHusbandIncomes.setIncomeTypeName("Other Husband Incomes");
        otherHusbandIncomes.setIncomeTypeId(utils.generateId(30));
        incomeTypeRepository.save(otherHusbandIncomes);

        IncomeTypeEntity otherWifeIncomes = new IncomeTypeEntity();
        otherWifeIncomes.setUserDetails(userDetails);
        otherWifeIncomes.setIncomeTypeName("Other Wife Incomes");
        otherWifeIncomes.setIncomeTypeId(utils.generateId(30));
        incomeTypeRepository.save(otherWifeIncomes);

        IncomeTypeEntity otherIncomes = new IncomeTypeEntity();
        otherIncomes.setUserDetails(userDetails);
        otherIncomes.setIncomeTypeName("Other Incomes");
        otherIncomes.setIncomeTypeId(utils.generateId(30));
        incomeTypeRepository.save(otherIncomes);
    }

    public void createDemoIncomes(UserEntity userEntity) {
        List<IncomeTypeEntity> incomeTypeEntities = incomeTypeRepository.findAllByUserDetails(userEntity);

        // incomes past month
        IncomeEntity husbandIncomesPastMonth = new IncomeEntity();
        husbandIncomesPastMonth.setIncomeId(utils.generateId(30));
        husbandIncomesPastMonth.setDate(utils.pastMonthFirstDay());
        husbandIncomesPastMonth.setName("Husband salary");
        husbandIncomesPastMonth.setAmount(2000);
        husbandIncomesPastMonth.setIncomeTypeDetails(incomeTypeEntities.get(0));
        husbandIncomesPastMonth.setUserDetails(userEntity);
        incomeRepository.save(husbandIncomesPastMonth);

        IncomeEntity wifeIncomesPastMonth = new IncomeEntity();
        wifeIncomesPastMonth.setIncomeId(utils.generateId(30));
        wifeIncomesPastMonth.setDate(utils.pastMonthFirstDay());
        wifeIncomesPastMonth.setName("Wife salary");
        wifeIncomesPastMonth.setAmount(2000);
        wifeIncomesPastMonth.setIncomeTypeDetails(incomeTypeEntities.get(1));
        wifeIncomesPastMonth.setUserDetails(userEntity);
        incomeRepository.save(wifeIncomesPastMonth);

        // Incomes current month
        IncomeEntity husbandIncomesCurrentMonth = new IncomeEntity();
        husbandIncomesCurrentMonth.setIncomeId(utils.generateId(30));
        husbandIncomesCurrentMonth.setDate(utils.firstMonthDay());
        husbandIncomesCurrentMonth.setName("Husband salary");
        husbandIncomesCurrentMonth.setAmount(2000);
        husbandIncomesCurrentMonth.setIncomeTypeDetails(incomeTypeEntities.get(0));
        husbandIncomesCurrentMonth.setUserDetails(userEntity);
        incomeRepository.save(husbandIncomesCurrentMonth);

        IncomeEntity wifeIncomesCurrentMonth = new IncomeEntity();
        wifeIncomesCurrentMonth.setIncomeId(utils.generateId(30));
        wifeIncomesCurrentMonth.setDate(utils.firstMonthDay());
        wifeIncomesCurrentMonth.setName("Wife salary");
        wifeIncomesCurrentMonth.setAmount(2000);
        wifeIncomesCurrentMonth.setIncomeTypeDetails(incomeTypeEntities.get(1));
        wifeIncomesCurrentMonth.setUserDetails(userEntity);
        incomeRepository.save(wifeIncomesCurrentMonth);


    }

    public void createDemoExpenses(UserEntity userEntity) {
        List<ExpenseTypeEntity> expenseTypeEntities = expenseTypeRepository.findAllByUserDetails(userEntity);

        // expenses past month
        ExpenseEntity clothingAndFootwear = new ExpenseEntity();
        clothingAndFootwear.setExpenseId(utils.generateId(30));
        clothingAndFootwear.setDate(utils.pastMonthFirstDay().plusDays(2));
        clothingAndFootwear.setName("New Clothing and Footwear");
        clothingAndFootwear.setAmount(300.0);
        clothingAndFootwear.setExpenseTypeDetails(expenseTypeEntities.get(2));
        clothingAndFootwear.setUserDetails(userEntity);
        expenseRepository.save(clothingAndFootwear);

        ExpenseEntity credit = new ExpenseEntity();
        credit.setExpenseId(utils.generateId(30));
        credit.setDate(utils.pastMonthFirstDay());
        credit.setName("Home loan");
        credit.setAmount(600.0);
        credit.setExpenseTypeDetails(expenseTypeEntities.get(4));
        credit.setUserDetails(userEntity);
        expenseRepository.save(credit);

        ExpenseEntity food = new ExpenseEntity();
        food.setExpenseId(utils.generateId(30));
        food.setDate(utils.pastMonthFirstDay().plusDays(5));
        food.setName("Food for the week");
        food.setAmount(150.0);
        food.setExpenseTypeDetails(expenseTypeEntities.get(5));
        food.setUserDetails(userEntity);
        expenseRepository.save(food);

        ExpenseEntity fuel = new ExpenseEntity();
        fuel.setExpenseId(utils.generateId(30));
        fuel.setDate(utils.pastMonthFirstDay().plusDays(6));
        fuel.setName("Fuel");
        fuel.setAmount(100.0);
        fuel.setExpenseTypeDetails(expenseTypeEntities.get(6));
        fuel.setUserDetails(userEntity);
        expenseRepository.save(fuel);

        ExpenseEntity health = new ExpenseEntity();
        health.setExpenseId(utils.generateId(30));
        health.setDate(utils.pastMonthFirstDay().plusDays(7));
        health.setName("Vitamins");
        health.setAmount(200.0);
        health.setExpenseTypeDetails(expenseTypeEntities.get(8));
        health.setUserDetails(userEntity);
        expenseRepository.save(health);

        ExpenseEntity entertainments = new ExpenseEntity();
        entertainments.setExpenseId(utils.generateId(30));
        entertainments.setDate(utils.pastMonthFirstDay().plusDays(7));
        entertainments.setName("Weekend with friends");
        entertainments.setAmount(200.0);
        entertainments.setExpenseTypeDetails(expenseTypeEntities.get(8));
        entertainments.setUserDetails(userEntity);
        expenseRepository.save(entertainments);

        // Expenses current month
        ExpenseEntity carMaintenanceCurrentMonth = new ExpenseEntity();
        carMaintenanceCurrentMonth.setExpenseId(utils.generateId(30));
        carMaintenanceCurrentMonth.setDate(utils.firstMonthDay().plusDays(1));
        carMaintenanceCurrentMonth.setName("New tires");
        carMaintenanceCurrentMonth.setAmount(800.0);
        carMaintenanceCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(0));
        carMaintenanceCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(carMaintenanceCurrentMonth);

        ExpenseEntity clothingAndFootwearCurrentMonth = new ExpenseEntity();
        clothingAndFootwearCurrentMonth.setExpenseId(utils.generateId(30));
        clothingAndFootwearCurrentMonth.setDate(utils.firstMonthDay().plusDays(2));
        clothingAndFootwearCurrentMonth.setName("New Clothing and Footwear");
        clothingAndFootwearCurrentMonth.setAmount(400.0);
        clothingAndFootwearCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(2));
        clothingAndFootwearCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(clothingAndFootwearCurrentMonth);

        ExpenseEntity creditCurrentMonth = new ExpenseEntity();
        creditCurrentMonth.setExpenseId(utils.generateId(30));
        creditCurrentMonth.setDate(utils.firstMonthDay().plusDays(3));
        creditCurrentMonth.setName("Home load");
        creditCurrentMonth.setAmount(600.0);
        creditCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(4));
        creditCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(creditCurrentMonth);

        ExpenseEntity foodCurrentMonth = new ExpenseEntity();
        foodCurrentMonth.setExpenseId(utils.generateId(30));
        foodCurrentMonth.setDate(utils.firstMonthDay().plusDays(3));
        foodCurrentMonth.setName("Food for the week");
        foodCurrentMonth.setAmount(150.0);
        foodCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(5));
        foodCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(foodCurrentMonth);

        ExpenseEntity fuelCurrentMonth = new ExpenseEntity();
        fuelCurrentMonth.setExpenseId(utils.generateId(30));
        fuelCurrentMonth.setDate(utils.firstMonthDay().plusDays(5));
        fuelCurrentMonth.setName("Fuel");
        fuelCurrentMonth.setAmount(100.0);
        fuelCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(6));
        fuelCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(fuelCurrentMonth);

        ExpenseEntity healthCurrentMonth = new ExpenseEntity();
        healthCurrentMonth.setExpenseId(utils.generateId(30));
        healthCurrentMonth.setDate(utils.firstMonthDay().plusDays(5));
        healthCurrentMonth.setName("Vitamins");
        healthCurrentMonth.setAmount(200.0);
        healthCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(8));
        healthCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(healthCurrentMonth);

        ExpenseEntity entertainmentsCurrentMonth = new ExpenseEntity();
        entertainmentsCurrentMonth.setExpenseId(utils.generateId(30));
        entertainmentsCurrentMonth.setDate(utils.firstMonthDay().plusDays(7));
        entertainmentsCurrentMonth.setName("Weekend with friends");
        entertainmentsCurrentMonth.setAmount(200.0);
        entertainmentsCurrentMonth.setExpenseTypeDetails(expenseTypeEntities.get(8));
        entertainmentsCurrentMonth.setUserDetails(userEntity);
        expenseRepository.save(entertainmentsCurrentMonth);


    }


}
