package com.upondev.famfin.app.ws.ui.controller;

import com.upondev.famfin.app.ws.exeptions.IncomeTypeServiceException;
import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.service.impl.IncomeTypeServiceImpl;
import com.upondev.famfin.app.ws.ui.model.request.IncomeTypeRequestModel;
import com.upondev.famfin.app.ws.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@RestController // Receive HTTP requests
@RequestMapping("users/{userId}/incomeType")
public class IncomeTypeController {

    final IncomeTypeServiceImpl incomeTypeService;

    public IncomeTypeController(IncomeTypeServiceImpl incomeTypeService) {
        this.incomeTypeService = incomeTypeService;
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType
    @PostMapping()
    public IncomeTypeRest createIncomeType(
            @PathVariable String userId,
            @RequestBody IncomeTypeRequestModel incomeTypeDetails) {
        if (incomeTypeDetails.getIncomeTypeName().isEmpty())
            throw new IncomeTypeServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        IncomeTypeEntity createdIncomeType = incomeTypeService.createIncomeType(userId, incomeTypeDetails.getIncomeTypeName());
        return new ModelMapper().map(createdIncomeType, IncomeTypeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}
    @PutMapping(path = "/{incomeTypeId}")
    public IncomeTypeRest updateIncomeType(
            @PathVariable String userId,
            @PathVariable String incomeTypeId,
            @RequestBody IncomeTypeRequestModel incomeTypeDetails) {
        IncomeTypeEntity updatedIncomeType = incomeTypeService.updateIncomeType(userId, incomeTypeId, incomeTypeDetails.getIncomeTypeName());
        return new ModelMapper().map(updatedIncomeType, IncomeTypeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}
    @DeleteMapping(path = "/{incomeTypeId}")
    public OperationStatusModel deleteIncomeType(
            @PathVariable String userId,
            @PathVariable String incomeTypeId) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        incomeTypeService.deleteIncomeType(userId, incomeTypeId);

        operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return operationStatus;
    }

    //http://localhost:8080/app-ws/users/{userId}/incomeType
    @GetMapping()
    public List<IncomeTypeRest> getIncomeTypes(@PathVariable String userId) {
        List<IncomeTypeRest> incomeTypeRests = new ArrayList<>();
        List<IncomeTypeEntity> incomeTypes = incomeTypeService.getIncomeTypes(userId);

        if (incomeTypes != null && !incomeTypes.isEmpty()) {
            Type listType = new TypeToken<List<IncomeTypeRest>>() {
            }.getType();
            incomeTypeRests = new ModelMapper().map(incomeTypes, listType);
        }
        return incomeTypeRests;
    }

    // http://localhost:8080/app-ws/users/{userId}/incomeType/{incomeTypeId}
    @GetMapping(path = "/{incomeTypeId}")
    public IncomeTypeRest getIncomeType(
            @PathVariable String userId,
            @PathVariable String incomeTypeId) {
        IncomeTypeEntity incomeTypeEntity = incomeTypeService.getIncomeType(userId, incomeTypeId);
        return new ModelMapper().map(incomeTypeEntity, IncomeTypeRest.class);
    }

}
