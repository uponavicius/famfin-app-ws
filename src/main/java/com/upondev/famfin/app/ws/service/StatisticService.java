package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.ui.model.response.StatisticRest;

import java.time.LocalDate;
import java.util.Date;


public interface StatisticService {
    StatisticRest getSharedStatistic(String userId);

    StatisticRest getBalance(String userId, LocalDate dateFrom, LocalDate DateTo);

    StatisticRest getIncomesStatistic(String userId, LocalDate dateFrom,  LocalDate dateTo);

    StatisticRest getExpenseStatistic(String userId, LocalDate dateFrom,  LocalDate dateTo);

}
