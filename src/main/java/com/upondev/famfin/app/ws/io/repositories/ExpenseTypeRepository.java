package com.upondev.famfin.app.ws.io.repositories;

import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseTypeRepository extends CrudRepository<ExpenseTypeEntity, Long> {
    ExpenseTypeEntity findByExpenseTypeNameAndUserDetails(String typeName, UserEntity userDetails);

    ExpenseTypeEntity findByExpenseTypeIdAndUserDetails(String typeName, UserEntity userDetails);

    List<ExpenseTypeEntity> findAllByUserDetails(UserEntity userDetails);
}
