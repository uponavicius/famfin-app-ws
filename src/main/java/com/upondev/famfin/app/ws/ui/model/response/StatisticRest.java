package com.upondev.famfin.app.ws.ui.model.response;

import java.util.Map;

public class StatisticRest {
    private double currentMonthIncomes;
    private double currentMonthExpenses;
    private double currentMonthBalance;
    private double allTimeBalance;
    private Map<String, Double> incomesByMonth;
    private Map<String, Double> expensesByMonth;
    private Map<String, Double> incomesStatistic;
    private Map<String, Double> expenseStatistic;

    public double getCurrentMonthIncomes() {
        return currentMonthIncomes;
    }

    public void setCurrentMonthIncomes(double currentMonthIncomes) {
        this.currentMonthIncomes = currentMonthIncomes;
    }

    public double getCurrentMonthExpenses() {
        return currentMonthExpenses;
    }

    public void setCurrentMonthExpenses(double currentMonthExpenses) {
        this.currentMonthExpenses = currentMonthExpenses;
    }

    public double getCurrentMonthBalance() {
        return currentMonthBalance;
    }

    public void setCurrentMonthBalance(double currentMonthBalance) {
        this.currentMonthBalance = currentMonthBalance;
    }

    public double getAllTimeBalance() {
        return allTimeBalance;
    }

    public void setAllTimeBalance(double allTimeBalance) {
        this.allTimeBalance = allTimeBalance;
    }

    public Map<String, Double> getIncomesByMonth() {
        return incomesByMonth;
    }

    public void setIncomesByMonth(Map<String, Double> incomesByMonth) {
        this.incomesByMonth = incomesByMonth;
    }

    public Map<String, Double> getExpensesByMonth() {
        return expensesByMonth;
    }

    public void setExpensesByMonth(Map<String, Double> expensesByMonth) {
        this.expensesByMonth = expensesByMonth;
    }

    public Map<String, Double> getIncomesStatistic() {
        return incomesStatistic;
    }

    public void setIncomesStatistic(Map<String, Double> incomesStatistic) {
        this.incomesStatistic = incomesStatistic;
    }

    public Map<String, Double> getExpenseStatistic() {
        return expenseStatistic;
    }

    public void setExpenseStatistic(Map<String, Double> expenseStatistic) {
        this.expenseStatistic = expenseStatistic;
    }
}
