package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.exeptions.ExpenseTypeServiceException;
import com.upondev.famfin.app.ws.exeptions.UserServiceException;
import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.ExpenseTypeRepository;
import com.upondev.famfin.app.ws.service.ExpenseTypeService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.ServiceUtils;
import com.upondev.famfin.app.ws.ui.model.response.ErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseTypeServiceImpl implements ExpenseTypeService {

    final ExpenseTypeRepository expenseTypeRepository;
    final Utils utils;
    final ServiceUtils serviceUtils;

    public ExpenseTypeServiceImpl(ExpenseTypeRepository expenseTypeRepository, Utils utils, ServiceUtils serviceUtils) {
        this.expenseTypeRepository = expenseTypeRepository;
        this.utils = utils;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public ExpenseTypeEntity createExpenseType(String userId, String expenseTypeName) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        if (expenseTypeRepository.findByExpenseTypeNameAndUserDetails(expenseTypeName, userDetails) != null)
            throw new ExpenseTypeServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());

        ExpenseTypeEntity expenseTypeEntity = new ExpenseTypeEntity();
        expenseTypeEntity.setExpenseTypeName(expenseTypeName);
        expenseTypeEntity.setExpenseTypeId(utils.generateId(30));
        expenseTypeEntity.setUserDetails(userDetails);

        return expenseTypeRepository.save(expenseTypeEntity);
    }

    @Override
    public ExpenseTypeEntity updateExpenseType(String userId, String expenseTypeId, String expenseTypeName) {
        if (expenseTypeName.isEmpty())
            throw new ExpenseTypeServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        ExpenseTypeEntity checkExpenseType = expenseTypeRepository.findByExpenseTypeNameAndUserDetails(expenseTypeName, userDetails);
        if (checkExpenseType != null)
            throw new UserServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());

        ExpenseTypeEntity expenseTypeEntity = serviceUtils.getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);
        expenseTypeEntity.setExpenseTypeName(expenseTypeName);

        return expenseTypeRepository.save(expenseTypeEntity);
    }

    @Override
    public void deleteExpenseType(String userId, String expenseTypeId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        if (userDetails == null)
            throw new UserServiceException(ErrorMessages.NO_USER_FOUND.getErrorMessage());

        ExpenseTypeEntity expenseTypeEntity =
                expenseTypeRepository.findByExpenseTypeIdAndUserDetails(expenseTypeId, userDetails);
        if (expenseTypeEntity == null)
            throw new ExpenseTypeServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        expenseTypeRepository.delete(expenseTypeEntity);
    }

    @Override
    public List<ExpenseTypeEntity> getExpenseTypes(String userId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return expenseTypeRepository.findAllByUserDetails(userDetails);
    }

    @Override
    public ExpenseTypeEntity getExpenseTypeById(String userId, String expenseTypeId) {
        return serviceUtils.getExpenseTypeEntityByUserIdAndExpenseTypeId(userId, expenseTypeId);
    }


}
