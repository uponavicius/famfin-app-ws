package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;

import java.util.List;

public interface IncomeTypeService {

    IncomeTypeEntity createIncomeType(String userId, String typeName);

    IncomeTypeEntity updateIncomeType(String userId, String incomeTypeId, String incomeTypeName);

    void deleteIncomeType(String userId, String incomeTypeId);

    List<IncomeTypeEntity> getIncomeTypes(String userId);

    IncomeTypeEntity getIncomeType(String userId, String publicIncomeTypeId);
}
