package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service("emailSenderService")
public class EmailServiceImpl implements EmailService {
    // https://mkyong.com/spring-boot/spring-boot-how-to-send-email-via-smtp/
    private final JavaMailSender javaMailSender;

    final String SUBJECT_VERIFY_EMAIL = "One last step to complete your registration with FamFinApp";
    final String SUBJECT_PASSWORD_RESET_REQUEST = "Password reset request - FamFinApp";

    // TODO viena nuoroda pasalinti keliant projekta i serveri
    final String VERIFY_EMAIL_BODY = "Please verify your email address. " + "\n"
            + "Thank you for registering with our mobile app. To complete registration process and be able to log in,"
            + " open then the following URL in your browser window: " + "\n\n"
            + "If project running on IDEA" + "\n"
            + "http://localhost:8080/app-ws/users/email-verification?token=$tokenValue" + "\n\n"
            + "If project running on Apatche Tomcat" + "\n"
            + "http://localhost:8080/verification-service/email-verification.html?token=$tokenValue" + "\n\n"
            + "Thank you! And we are waiting for you inside!";

    // TODO viena nuoroda pasalinti keliant projekta i serveri
    final String PASSWORD_RESET_REQUEST_BODY = "Hello, $firstName" + "!" + "\n"
            + "Someone has requested to reset your password with FamFin project. If it were not you, please ignore it."
            + " Otherwise please open the link below in your browser window to set a new password:" + "\n\n"
            + "If project running on IDEA" + "\n"
            + "http://localhost:8080/app-ws/users/password-reset?token=$tokenValue" + "\n\n"
            + "If project running on Apatche Tomcat" + "\n"
            + "http://localhost:8080/verification-service/password-reset.html?token=$tokenValue" + "\n\n"
            + "Thank you!";

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void verifyEmail(UserEntity userEntity) {
        String textBody = VERIFY_EMAIL_BODY.replace("$tokenValue", userEntity.getEmailVerificationToken());
        try {
            sendEmail(userEntity.getEmail(), SUBJECT_VERIFY_EMAIL, textBody);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean sendPasswordResetRequest(String firstName, String email, String token) {
        boolean isPasswordReset = false;

        String textBody = PASSWORD_RESET_REQUEST_BODY.replace("$firstName", firstName);
        textBody = textBody.replace("$tokenValue", token);
        try {
            sendEmail(email, SUBJECT_PASSWORD_RESET_REQUEST, textBody);
            isPasswordReset = true;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return isPasswordReset;
    }

    @Override
    public void sendEmail(String email, String subject, String textBody) throws MessagingException {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);
        msg.setSubject(subject);
        msg.setText(textBody);
        javaMailSender.send(msg);
    }


}
