package com.upondev.famfin.app.ws.ui.model.request;

import java.util.Map;

public class UserDetailsRequestModel {
    // This class using convert income JSON model to JAVA object
    // Class fields must match to JSON payload

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String name;
    private Map<String, Object> battery;

}
