package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserEntity createUser(String firstName, String lastName, String email, String password);

    UserEntity getUser(String email);

    UserEntity getUserByUserId(String userId);

    UserEntity updateUser(String userId, String firstName, String lastName);

    void deleteUser(String userId);

    boolean verifyEmailToken(String token);

    boolean requestPasswordReset(String email);

    boolean resetPassword(String token, String password);
}
