package com.upondev.famfin.app.ws.io.repositories;

import com.upondev.famfin.app.ws.io.entity.IncomeEntity;
import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface IncomeRepository extends CrudRepository<IncomeEntity, Long> {

    IncomeEntity findByIncomeIdAndIncomeTypeDetailsAndUserDetails(
            String incomeId, IncomeTypeEntity incomeTypeDetails, UserEntity userDetails);

    IncomeEntity findByIncomeIdAndUserDetails(String incomeId, UserEntity userDetails);

    List<IncomeEntity> findAllByUserDetails(UserEntity userDetails);

    List<IncomeEntity> findByDateBetweenAndUserDetails(LocalDate dateFrom, LocalDate dateTo, UserEntity userDetails);

    List<IncomeEntity> findByDateBetweenAndUserDetailsAndIncomeTypeDetails(
            LocalDate dateFrom, LocalDate dateTo, UserEntity userDetails, IncomeTypeEntity incomeTypeDetails);



}
