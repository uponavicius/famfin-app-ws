package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.IncomeEntity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface IncomeService {

    IncomeEntity createIncome(String userId, String incomeTypeId, LocalDate date, String name, double amount);

    IncomeEntity updateIncome(String userId, String incomeTypeId, String incomeId, LocalDate date, String name, double amount);

    void deleteIncome(String userId, String incomeTypeId, String incomeId);

    List<IncomeEntity> getIncomes(String userId);

    IncomeEntity getIncome(String userId, String incomeTypeId, String incomeId);

    List<IncomeEntity> getIncomesBetweenTwoDays(String userId, LocalDate dateFrom, LocalDate dateTo);

    List<IncomeEntity> getIncomesBetweenTwoDaysAndIncomeType(String userId, String incomeTypeId, LocalDate dateFrom, LocalDate dateTo);

}

