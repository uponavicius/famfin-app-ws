package com.upondev.famfin.app.ws.ui.controller;

import com.upondev.famfin.app.ws.exeptions.ExpenseTypeServiceException;
import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.service.impl.ExpenseTypeServiceImpl;
import com.upondev.famfin.app.ws.ui.model.request.ExpenseTypeRequestModel;
import com.upondev.famfin.app.ws.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("users/{userId}/expenseType")
public class ExpenseTypeController {

    final ExpenseTypeServiceImpl expenseTypeService;

    public ExpenseTypeController(ExpenseTypeServiceImpl expenseTypeService) {
        this.expenseTypeService = expenseTypeService;
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType
    @PostMapping()
    public ExpenseTypeRest createExpenseType(
            @PathVariable String userId,
            @RequestBody ExpenseTypeRequestModel expenseTypeDetails) {
        if (expenseTypeDetails.getExpenseTypeName().isEmpty())
            throw new ExpenseTypeServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        ExpenseTypeEntity createdExpenseType = expenseTypeService.createExpenseType(userId, expenseTypeDetails.getExpenseTypeName());

        return new ModelMapper().map(createdExpenseType, ExpenseTypeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}
    @PutMapping(path = "/{expenseTypeId}")
    public ExpenseTypeRest updateExpenseType(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @RequestBody ExpenseTypeRequestModel expenseDetails) {

        ExpenseTypeEntity updatedExpenseType = expenseTypeService
                .updateExpenseType(userId, expenseTypeId, expenseDetails.getExpenseTypeName());

        return new ModelMapper().map(updatedExpenseType, ExpenseTypeRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}
    @DeleteMapping(path = "/{expenseTypeId}")
    public OperationStatusModel deleteExpenseType(
            @PathVariable String userId,
            @PathVariable String expenseTypeId) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        expenseTypeService.deleteExpenseType(userId, expenseTypeId);

        operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return operationStatus;
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType
    @GetMapping()
    public List<ExpenseTypeRest> getExpenseTypes(@PathVariable String userId) {
        List<ExpenseTypeRest> expenseTypeRests = new ArrayList<>();
        List<ExpenseTypeEntity> expenseTypes = expenseTypeService.getExpenseTypes(userId);

        if (expenseTypes != null && !expenseTypes.isEmpty()) {
            Type listType = new TypeToken<List<ExpenseTypeRest>>() {
            }.getType();
            expenseTypeRests = new ModelMapper().map(expenseTypes, listType);
        }


        return expenseTypeRests;
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}
    @GetMapping(path = "/{expenseTypeId}")
    public ExpenseTypeRest getExpenseType(
            @PathVariable String userId,
            @PathVariable String expenseTypeId) {
        ExpenseTypeEntity expenseTypeEntity = expenseTypeService.getExpenseTypeById(userId, expenseTypeId);
        return new ModelMapper().map(expenseTypeEntity, ExpenseTypeRest.class);
    }
}
