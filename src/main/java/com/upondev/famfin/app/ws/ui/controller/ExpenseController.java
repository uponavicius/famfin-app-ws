package com.upondev.famfin.app.ws.ui.controller;


import com.upondev.famfin.app.ws.exeptions.ExpenseServiceException;
import com.upondev.famfin.app.ws.io.entity.ExpenseEntity;
import com.upondev.famfin.app.ws.service.impl.ExpenseServiceImpl;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.ui.model.request.ExpenseRequestModel;
import com.upondev.famfin.app.ws.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController // Receive HTTP requests
@RequestMapping("users/{userId}")
public class ExpenseController {

    final ExpenseServiceImpl expenseService;
    final Utils utils;

    public ExpenseController(ExpenseServiceImpl expenseService, Utils utils) {
        this.expenseService = expenseService;
        this.utils = utils;
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}/expense
    @PostMapping(path = "/expenseType/{expenseTypeId}/expense")
    public ExpenseRest createExpense(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @RequestBody ExpenseRequestModel expenseRequestDetails) {
        checkExpenseRequestModelData(expenseRequestDetails);

        ExpenseEntity createdExpense = expenseService.createExpense(
                userId,
                expenseTypeId,
                expenseRequestDetails.getDate(),
                expenseRequestDetails.getName(),
                expenseRequestDetails.getAmount()
        );

        return new ModelMapper().map(createdExpense, ExpenseRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}/expense/{expenseId}
    @PutMapping(path = "/expenseType/{expenseTypeId}/expense/{expenseId}")
    public ExpenseRest updateExpense(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @PathVariable String expenseId,
            @RequestBody ExpenseRequestModel expenseRequestDetails) {
        checkExpenseRequestModelData(expenseRequestDetails);

        ExpenseEntity updatedExpense = expenseService.updateExpense(
                userId,
                expenseTypeId,
                expenseId,
                expenseRequestDetails.getDate(),
                expenseRequestDetails.getName(),
                expenseRequestDetails.getAmount()
        );

        return new ModelMapper().map(updatedExpense, ExpenseRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}/expense/{expenseId}
    @DeleteMapping(path = "/expenseType/{expenseTypeId}/expense/{expenseId}")
    public OperationStatusModel deleteExpense(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @PathVariable String expenseId) {
        OperationStatusModel returnOperationStatus = new OperationStatusModel();
        returnOperationStatus.setOperationName(RequestOperationName.DELETE.name());

        expenseService.delete(userId, expenseTypeId, expenseId);

        returnOperationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return returnOperationStatus;
    }

    // http://localhost:8080/app-ws/users/{userId}/expense
    @GetMapping(path = "expense")
    public List<ExpenseRest> getAllExpenses(@PathVariable String userId) {
        List<ExpenseEntity> expenses = expenseService.getExpenses(userId);
        return expenseEntityToExpenseRest(expenses);
    }

    // http://localhost:8080/app-ws/users/{userId}/expense/{expenseId}
    @GetMapping(path = "/expenseType/{expenseTypeId}/expense/{expenseId}")
    public ExpenseRest getExpense(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @PathVariable String expenseId) {
        ExpenseEntity expenses = expenseService.getExpense(userId, expenseTypeId, expenseId);
        return new ModelMapper().map(expenses, ExpenseRest.class);
    }

    // http://localhost:8080/app-ws/users/{userId}/expense?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "expenses")
    public List<ExpenseRest> getExpensesBetweenTwoDays(
            @PathVariable String userId,
            @RequestParam String dateFrom,
            @RequestParam String dateTo) {
        List<ExpenseEntity> expenses = expenseService.getExpensesBetweenTwoDays(
                userId,
                utils.stringToLocalDate(dateFrom),
                utils.stringToLocalDate(dateTo)
        );

        return expenseEntityToExpenseRest(expenses);
    }

    // http://localhost:8080/app-ws/users/{userId}/expenseType/{expenseTypeId}/expense?dateFrom=2020-01-01&dateTo=2020-06-16
    @GetMapping(path = "/expenseType/{expenseTypeId}/expense")
    public List<ExpenseRest> getExpensesBetweenTwoDaysAndType(
            @PathVariable String userId,
            @PathVariable String expenseTypeId,
            @RequestParam LocalDate dateFrom,
            @RequestParam LocalDate dateTo) {

        List<ExpenseEntity> expenses = expenseService.
                getExpensesBetweenTwoDaysAndExpenseType(userId, expenseTypeId, dateFrom, dateTo);

        return expenseEntityToExpenseRest(expenses);
    }

    private List<ExpenseRest> expenseEntityToExpenseRest(List<ExpenseEntity> expenses) {
        List<ExpenseRest> expenseRests = new ArrayList<>();
        if (expenses != null && !expenses.isEmpty()) {
            Type listType = new TypeToken<List<ExpenseRest>>() {
            }.getType();
            expenseRests = new ModelMapper().map(expenses, listType);
        }
        return expenseRests;
    }

    private void checkExpenseRequestModelData(ExpenseRequestModel expenseRequestDetails) {
        if (expenseRequestDetails.getDate() == null || expenseRequestDetails.getName().isEmpty())
            throw new ExpenseServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
        if (expenseRequestDetails.getAmount() <= 0)
            throw new ExpenseServiceException(ErrorMessages.AMOUNT_GREATER_THAN_ZERO.getErrorMessage());
    }

}
