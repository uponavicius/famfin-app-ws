package com.upondev.famfin.app.ws.ui.model.request;

public class ExpenseTypeRequestModel {

    private String expenseTypeName;

    public String getExpenseTypeName() {
        return expenseTypeName;
    }

    public void setExpenseTypeName(String expenseTypeName) {
        this.expenseTypeName = expenseTypeName;
    }
}
