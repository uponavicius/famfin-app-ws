package com.upondev.famfin.app.ws.io.repositories;

import com.upondev.famfin.app.ws.io.entity.ExpenseEntity;
import com.upondev.famfin.app.ws.io.entity.ExpenseTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface ExpenseRepository extends CrudRepository<ExpenseEntity, Long> {
    ExpenseEntity findByExpenseIdAndExpenseTypeDetailsAndUserDetails(
            String expenseTypeId, ExpenseTypeEntity expenseTypeDetails, UserEntity userDetails);

    ExpenseEntity findByExpenseIdAndUserDetails(String incomeId, UserEntity userDetails);

    List<ExpenseEntity> findAllByUserDetails(UserEntity userDetails);

    List<ExpenseEntity> findByDateBetweenAndUserDetails(LocalDate dateFom, LocalDate dateTo, UserEntity userDetails);

    List<ExpenseEntity> findByDateBetweenAndUserDetailsAndExpenseTypeDetails(
            LocalDate dateFom, LocalDate dateTo, UserEntity userDetails, ExpenseTypeEntity expenseTypeDetails);
}
