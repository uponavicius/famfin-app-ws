package com.upondev.famfin.app.ws.ui.model.response;

public enum  RequestOperationStatus {
    ERROR,
    SUCCESS
}
