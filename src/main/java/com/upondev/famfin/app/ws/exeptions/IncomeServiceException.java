package com.upondev.famfin.app.ws.exeptions;

public class IncomeServiceException extends RuntimeException {
    private static final long serialVersionUID = 4624354620593742447L;

    public IncomeServiceException(String message) {
        super(message);
    }
}
