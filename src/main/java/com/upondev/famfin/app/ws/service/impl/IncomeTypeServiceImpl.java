package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.exeptions.IncomeTypeServiceException;
import com.upondev.famfin.app.ws.exeptions.UserServiceException;
import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.IncomeTypeRepository;
import com.upondev.famfin.app.ws.service.IncomeTypeService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import com.upondev.famfin.app.ws.ui.model.response.ErrorMessages;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncomeTypeServiceImpl implements IncomeTypeService {

    final IncomeTypeRepository incomeTypeRepository;
    final Utils utils;
    final ServiceUtilsImpl serviceUtils;

    public IncomeTypeServiceImpl(IncomeTypeRepository incomeTypeRepository, Utils utils, ServiceUtilsImpl serviceUtils) {
        this.incomeTypeRepository = incomeTypeRepository;
        this.utils = utils;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public IncomeTypeEntity createIncomeType(String userId, String typeName) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        if (incomeTypeRepository.findByIncomeTypeNameAndUserDetails(typeName, userDetails) != null)
            throw new UserServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());

        IncomeTypeEntity incomeTypeEntity = new IncomeTypeEntity();
        incomeTypeEntity.setIncomeTypeName(typeName);
        incomeTypeEntity.setIncomeTypeId(utils.generateId(30));
        incomeTypeEntity.setUserDetails(userDetails);

        return incomeTypeRepository.save(incomeTypeEntity);
    }

    @Override
    public IncomeTypeEntity updateIncomeType(String userId, String incomeTypeId, String typeName) {
        if (typeName.isEmpty())
            throw new IncomeTypeServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        IncomeTypeEntity checkIncomeType = incomeTypeRepository.findByIncomeTypeNameAndUserDetails(typeName, userDetails);
        if (checkIncomeType != null)
            throw new UserServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());

        IncomeTypeEntity incomeTypeEntity = serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);
        incomeTypeEntity.setIncomeTypeName(typeName);

        return incomeTypeRepository.save(incomeTypeEntity);
    }

    @Override
    public void deleteIncomeType(String userId, String incomeTypeId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);

        if (userDetails == null)
            throw new UserServiceException(ErrorMessages.NO_USER_FOUND.getErrorMessage());

        IncomeTypeEntity incomeTypeEntity =
                serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);

        if (incomeTypeEntity == null)
            throw new IncomeTypeServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

        incomeTypeRepository.delete(incomeTypeEntity);
    }

    @Override
    public List<IncomeTypeEntity> getIncomeTypes(String userId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return incomeTypeRepository.findAllByUserDetails(userDetails);
    }

    @Override
    public IncomeTypeEntity getIncomeType(String userId, String incomeTypeId) {
        return serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);
    }


}
