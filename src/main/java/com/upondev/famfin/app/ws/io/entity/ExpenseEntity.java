package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity(name = "expenses")
@ToString
public class ExpenseEntity implements Serializable {
    private static final long serialVersionUID = -8969688817096202068L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String expenseId;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Getter
    @Setter
    private LocalDate date;

    @Column(nullable = false, length = 50)
    @Getter
    @Setter
    private String name;

    @Column(nullable = false, length = 10)
    @Getter
    @Setter
    private Double amount;

    // Expense record can have just one income type
    @ManyToOne
    @JoinColumn(name = "expense_types_id")
    @Getter
    @Setter
    private ExpenseTypeEntity expenseTypeDetails;

    // Expense record can have just one owner
    @ManyToOne
    @JoinColumn(name = "users_id")
    @Getter
    @Setter
    private UserEntity userDetails;

}
