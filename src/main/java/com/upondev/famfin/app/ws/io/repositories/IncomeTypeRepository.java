package com.upondev.famfin.app.ws.io.repositories;

import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncomeTypeRepository extends CrudRepository<IncomeTypeEntity, Long> {

    IncomeTypeEntity findByIncomeTypeNameAndUserDetails(String typeName, UserEntity userDetails);

    IncomeTypeEntity findByIncomeTypeIdAndUserDetails(String incomeTypeId, UserEntity userDetails);

    List<IncomeTypeEntity> findAllByUserDetails(UserEntity userDetails);
}
