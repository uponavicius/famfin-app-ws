package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "expense_types")
@ToString
public class ExpenseTypeEntity implements Serializable {
    private static final long serialVersionUID = -8464709099476671794L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String expenseTypeId;

    @Column(nullable = false, length = 30)
    @Getter
    @Setter
    private String expenseTypeName;

    // Expense type can have a lot of expense records
    @OneToMany(mappedBy = "expenseTypeDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<ExpenseEntity> expenses;

    // Expense type can have just one owner
    @ManyToOne
    @JoinColumn(name = "users_id")
    @Getter
    @Setter
    private UserEntity userDetails;


}
