package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

@Entity(name = "incomes")
@ToString
public class IncomeEntity implements Serializable {
    private static final long serialVersionUID = -479824988268528442L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String incomeId;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Getter
    @Setter
    private LocalDate date;

    @Column(nullable = false, length = 50)
    @Getter
    @Setter
    private String name;

    @Column(nullable = false, length = 10)
    @Getter
    @Setter

    private double amount;

    // Income record can have just one income type
    @ManyToOne
    @JoinColumn(name = "income_types_id")
    @Getter
    @Setter
    private IncomeTypeEntity incomeTypeDetails;

    // Income record can have just one owner
    @ManyToOne
    @JoinColumn(name = "users_id")
    @Getter
    @Setter
    private UserEntity userDetails;

}
