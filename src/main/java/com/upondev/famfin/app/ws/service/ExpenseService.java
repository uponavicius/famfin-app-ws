package com.upondev.famfin.app.ws.service;

import com.upondev.famfin.app.ws.io.entity.ExpenseEntity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface ExpenseService {

    ExpenseEntity createExpense(String userId, String expenseTypeId, LocalDate date, String name, double amount);

    ExpenseEntity updateExpense(String userId, String expenseTypeId, String expenseId, LocalDate date, String name, double amount);

    void delete(String userId, String expenseTypeId, String expenseId);

    List<ExpenseEntity> getExpenses(String userId);

    ExpenseEntity getExpense(String userId, String expenseTypeEd, String expenseId);

    List<ExpenseEntity> getExpensesBetweenTwoDays(String userId, LocalDate dateFrom, LocalDate dateTo);

    List<ExpenseEntity> getExpensesBetweenTwoDaysAndExpenseType(String userId, String expenseTypeId, LocalDate dateFrom, LocalDate dateTo);
}
