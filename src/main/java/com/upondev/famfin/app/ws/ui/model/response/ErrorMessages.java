package com.upondev.famfin.app.ws.ui.model.response;

// http://www.appsdeveloperblog.com/handle-exceptions-spring-boot/
public enum ErrorMessages {
    MISSING_REQUIRED_FIELD("Please fill in all required fields"),
    RECORD_ALREADY_EXISTS("Record already exists"),
    INTERNAL_SERVER_ERROR("Internal server error"),
    NO_RECORD_FOUND("Record with provided id is not found"),
    AUTHENTICATION_FAILED("Authentication failed"),
    NO_USER_FOUND("User Not found, by user ID"),
    NO_INCOME_TYPE_FOUND("Income type not found"),
    NO_INCOME_FOUND("Income not found"),
    NO_EXPENSE_FOUND("Expense not found"),
    NO_EXPENSE_TYPE_FOUND("Expense type not found"),
    AMOUNT_GREATER_THAN_ZERO("Amount must to be greater than zero"),
    COULD_NOT_UPDATE_RECORD("Could not update record"),
    COULD_NOT_DELETE_RECORD("Could not delete record"),
    EMAIL_ADDRESS_NOT_VERIFIED("Email address could not be verified");

    private String errorMessage;

    ErrorMessages(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
