package com.upondev.famfin.app.ws.shared;

import com.upondev.famfin.app.ws.security.SecurityConstants;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Random;

@Component
public class Utils {
    private final Random RANDOM = new SecureRandom();
    private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public String generateId(int length) {
        return generateRandomString(length);
    }

    private String generateRandomString(int length) {
        StringBuilder generatedString = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            generatedString.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        // TODO cia kad butu tikrai unikalus id;
        // generatedString.append(System.currentTimeMillis());
        // generatedString.append(System.nanoTime());
        return new String(generatedString);
    }

    public static boolean hasTokenExpired(String token) {
        boolean isTokenExpired = false;

        try {
            Claims claims = Jwts
                    .parser()
                    .setSigningKey(SecurityConstants.getTokenSecret())
                    .parseClaimsJws(token)
                    .getBody();

            Date tokenExpirationDate = claims.getExpiration();
            Date todayDate = new Date();

            isTokenExpired = tokenExpirationDate.before(todayDate);
        } catch (ExpiredJwtException | SignatureException ex) {
            isTokenExpired = true;
        }
        return isTokenExpired;
    }

    public String generateEmailVerificationToken(String userId) {
        String token = Jwts.builder()
                .setSubject(userId)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret())
                .compact();
        return token;
    }

    public String generatePasswordResetToken(String userId) {
        String token = Jwts.builder()
                .setSubject(userId)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.PASSWORD_RESET_EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret())
                .compact();
        return token;
    }

    // public Date localDateToDate(LocalDate localDate) {
    //     Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    //     return date;
    // }

    public LocalDate firstMonthDay() {
        return LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
    }

    public LocalDate lastMonthDay() {
        return LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
    }

    public LocalDate pastMonthFirstDay() {
        return LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).minusMonths(1);
    }

    public Date stringToDate(String dateString) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public LocalDate stringToLocalDate (String dateString){
        LocalDate localDate = LocalDate.parse(dateString);
        return localDate;
    }

}
