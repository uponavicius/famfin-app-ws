package com.upondev.famfin.app.ws.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upondev.famfin.app.ws.SpringApplicationContext;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.service.UserService;
import com.upondev.famfin.app.ws.ui.model.request.UserLoginRequestModel;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    // When our service receive request authenticate user by username (email) and password, this methot will be trigered.
    // Json will included in in request body will be used UserLoginRequestModel Java class
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            UserLoginRequestModel creds = new ObjectMapper()
                    .readValue(req.getInputStream(), UserLoginRequestModel.class);

            // Then will be use authenticationManager to authenticate user
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            new ArrayList<>())
            );

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // If method attemptAuthentication() find user, then was called successfulAuthentication() method by spring framework (if user name and password correct).
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        String userName = ((User) auth.getPrincipal()).getUsername();
        //String tokenSecret = new SecurityContains().getTokenSecret();

        // Then app need communicate with protected resources, example get list of all users, update, or delete.
        // It will need to include this Json web token as header in to request, otherwise it not will be authorized.
        // token example - eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKb2UifQ.ipevRNuRP6HflG8cFKnmUPtypruRC4fb1DWtoLL62SY
        String token = Jwts.builder() //Jwts - Json Web Token library https://github.com/jwtk/jjwt
                .setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret())
                .compact();

        // Creating a bean
        UserService userService = (UserService) SpringApplicationContext.getBean("userServiceImpl");
        UserEntity userEntity = userService.getUser(userName);

        res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token); //Authorization header
        res.addHeader("UserId", userEntity.getUserId()); //  In header add user public userId;
        res.addHeader("FirstName", userEntity.getFirstName());
        res.addHeader("access-control-expose-headers", "Authorization, UserId, FirstName");


    }
}
