package com.upondev.famfin.app.ws.service.impl;

import com.upondev.famfin.app.ws.io.entity.IncomeEntity;
import com.upondev.famfin.app.ws.io.entity.IncomeTypeEntity;
import com.upondev.famfin.app.ws.io.entity.UserEntity;
import com.upondev.famfin.app.ws.io.repositories.IncomeRepository;
import com.upondev.famfin.app.ws.io.repositories.IncomeTypeRepository;
import com.upondev.famfin.app.ws.service.IncomeService;
import com.upondev.famfin.app.ws.shared.Utils;
import com.upondev.famfin.app.ws.shared.service.impl.ServiceUtilsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class IncomeServiceImpl implements IncomeService {

    final IncomeRepository incomeRepository;
    final IncomeTypeRepository incomeTypeRepository;
    final Utils utils;
    final ServiceUtilsImpl serviceUtils;

    public IncomeServiceImpl(IncomeRepository incomeRepository, IncomeTypeRepository incomeTypeRepository, Utils utils, ServiceUtilsImpl serviceUtils) {
        this.incomeRepository = incomeRepository;
        this.incomeTypeRepository = incomeTypeRepository;
        this.utils = utils;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public IncomeEntity createIncome(String userId, String incomeTypeId, LocalDate date, String name, double amount) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        IncomeTypeEntity incomeTypeDetails = serviceUtils.
                getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);

        IncomeEntity incomeEntity = new IncomeEntity();
        incomeEntity.setIncomeId(utils.generateId(30));
        incomeEntity.setDate(date);
        incomeEntity.setName(name);
        incomeEntity.setAmount(amount);
        incomeEntity.setUserDetails(userDetails);
        incomeEntity.setIncomeTypeDetails(incomeTypeDetails);

        return incomeRepository.save(incomeEntity);
    }

    @Override
    public IncomeEntity updateIncome(String userId, String incomeTypeId, String incomeId, LocalDate date, String name, double amount) {
        IncomeEntity incomeEntity = serviceUtils.
                getIncomeEntityByUserIdAndIncomeId(userId, incomeId);

        IncomeTypeEntity incomeTypeEntity = serviceUtils.getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);

        incomeEntity.setDate(date);
        incomeEntity.setName(name);
        incomeEntity.setAmount(amount);
        incomeEntity.setIncomeTypeDetails(incomeTypeEntity);

        return incomeRepository.save(incomeEntity);
    }

    @Override
    public void deleteIncome(String userId, String incomeTypeId, String incomeId) {
        IncomeEntity incomeEntity = serviceUtils.
                getIncomeEntityByUserIdAndIncomeTypeIdAndIncomeId(userId, incomeTypeId, incomeId);
        incomeRepository.delete(incomeEntity);
    }

    @Override
    public List<IncomeEntity> getIncomes(String userId) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return incomeRepository.findAllByUserDetails(userDetails);
    }

    @Override
    public IncomeEntity getIncome(String userId, String incomeTypeId, String incomeId) {
        return serviceUtils.getIncomeEntityByUserIdAndIncomeTypeIdAndIncomeId(userId, incomeTypeId, incomeId);
    }

    @Override
    public List<IncomeEntity> getIncomesBetweenTwoDays(String userId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        return incomeRepository.findByDateBetweenAndUserDetails(dateFrom, dateTo, userDetails);
    }

    @Override
    public List<IncomeEntity> getIncomesBetweenTwoDaysAndIncomeType(String userId, String incomeTypeId, LocalDate dateFrom, LocalDate dateTo) {
        UserEntity userDetails = serviceUtils.getUserEntityByUserId(userId);
        IncomeTypeEntity incomeTypeDetails = serviceUtils.
                getIncomeTypeEntityByUserIdAndIncomeTypeId(userId, incomeTypeId);

        return incomeRepository.
                findByDateBetweenAndUserDetailsAndIncomeTypeDetails(
                        dateFrom,
                        dateTo,
                        userDetails,
                        incomeTypeDetails
                );
    }

}




























