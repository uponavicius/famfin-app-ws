package com.upondev.famfin.app.ws.ui.model.response;

public class IncomeTypeRest {
    private String incomeTypeId;
    private String incomeTypeName;

    public String getIncomeTypeId() {
        return incomeTypeId;
    }

    public void setIncomeTypeId(String incomeTypeId) {
        this.incomeTypeId = incomeTypeId;
    }

    public String getIncomeTypeName() {
        return incomeTypeName;
    }

    public void setIncomeTypeName(String incomeTypeName) {
        this.incomeTypeName = incomeTypeName;
    }
}
