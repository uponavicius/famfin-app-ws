package com.upondev.famfin.app.ws.exeptions;

public class ExpenseTypeServiceException extends RuntimeException{
    private static final long serialVersionUID = 2792092795164512039L;

    public ExpenseTypeServiceException(String message) {
        super(message);
    }
}
