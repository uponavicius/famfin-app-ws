package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "income_types")
@ToString
public class IncomeTypeEntity implements Serializable {
    private static final long serialVersionUID = -2299434293513014286L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String incomeTypeId;

    @Column(nullable = false, length = 30)
    @Getter
    @Setter
    private String incomeTypeName;

    // Income type can have a lot of income records
    @OneToMany(mappedBy = "incomeTypeDetails", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<IncomeEntity> incomes;

    // Income type can have just one owner
    @ManyToOne
    @JoinColumn(name = "users_id")
    @Getter
    @Setter
    private UserEntity userDetails;

}
