package com.upondev.famfin.app.ws.exeptions;

public class IncomeTypeServiceException extends RuntimeException {
    private static final long serialVersionUID = 7001396064795565330L;

    public IncomeTypeServiceException(String message) {
        super(message);
    }
}
