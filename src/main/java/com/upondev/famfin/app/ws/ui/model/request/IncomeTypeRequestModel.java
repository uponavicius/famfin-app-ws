package com.upondev.famfin.app.ws.ui.model.request;

public class IncomeTypeRequestModel {

    private String incomeTypeName;

    public String getIncomeTypeName() {
        return incomeTypeName;
    }

    public void setIncomeTypeName(String incomeTypeName) {
        this.incomeTypeName = incomeTypeName;
    }
}
