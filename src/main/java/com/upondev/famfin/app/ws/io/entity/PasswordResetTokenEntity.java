package com.upondev.famfin.app.ws.io.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "password_reset_token")
@ToString
public class PasswordResetTokenEntity implements Serializable {
    private static final long serialVersionUID = -5708190706769067112L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @Getter
    @Setter
    private String token;

    @OneToOne()
    @JoinColumn(name = "users_id")
    @Getter
    @Setter
    private UserEntity userDetails;

}
